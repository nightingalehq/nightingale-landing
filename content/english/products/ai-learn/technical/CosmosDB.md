---
title : "Azure CosmosDB"
lastmod : "2018-02-17"
draft: true
---

This course will help you understand the core features of CosmosDB and cover focussed techniques to connect from a data science workflow.

## Azure CosmosDB for data science

CosmosDB (formerly DocumentDB) is a database as a service product in Azure. This non-relational data base features turnkey scalable geo-distribution, variable consistency models, schema-less writes and multiple query paradigms, but how do you do data science on it?

## Learning Outcomes

- Understand the core features of CosmosDB: scaling, consistency, replication, geo-distribution, non-relational data and schema-less operation.
- Learn focussed techniques to connect from a data science workflow, example service architectures for data science outputs and outline supported compute methods to scale analysis.

## What our training days look like

We pair up teaching you important theory with practical exercises to get you confident in your use of the conceptual frameworks and tools you'll be learning. You'll work with real datasets so you'll always be able to see how you can apply things in real life.

## What you'll need on the day

You'll need a device with internet and RDP allowed and ideally a camera. You'll be doing exercises so that you get familiar with the code and the concepts, as well as viewing the online course handouts, and accessing the training group's Slack channel. We use online coding environments so all you need to have installed is a web browser (for best results this should be a recent version of Chrome or Firefox).

## What you'll receive

Before the event we'll invite you into our Slack workspace and the private channel for your course. We'll share links, useful pre-reading, and more. This'll be a place you can ask questions during the course and post achievements afterwards.

On the day of the course, you'll be given access to our online materials and any lab environments, meaning you can spend more time learning and less time solving computer woes.

After the training, you'll have access to our online materials FOREVER. We'll also let you share the link inside your organisation. You'll also be able to access the lab environments for a week after the training so you can recap when you get back to work.
