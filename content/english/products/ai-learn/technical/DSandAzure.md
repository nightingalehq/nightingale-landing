---
title : "Data Science and Azure"
lastmod : "2018-02-17"
draft: true
---

An introduction to data science using Azure ML Studio.

## data science using Azure ML Studio

This one day training course focuses on the data science workflow using Azure ML to end up with working data science prototypes that can be called from all sorts of applications and systems.

## Learning Outcomes

Learn how to set up Azure ML
Connect and consume with data
Tidy up and prepare data with available tools, including SQL, R, and Python
Sampling your data so you have some data to use and some data for testing
Building some models that suit tackle your data science question
Evaluating models and picking the best model
Deploying your model to a web service
This isn't an in-depth introduction to data science but it gives you enough to get started and in an easy to use tool which you can use whilst you're learning the important concepts.

## What our training days look like

We pair up teaching you important theory with practical exercises to get you confident in your use of the conceptual frameworks and tools you'll be learning. You'll work with real datasets so you'll always be able to see how you can apply things in real life.

## What you'll need on the day

You'll need a device with internet and RDP allowed and ideally a camera. You'll be doing exercises so that you get familiar with the code and the concepts, as well as viewing the online course handouts, and accessing the training group's Slack channel. We use online coding environments so all you need to have installed is a web browser (for best results this should be a recent version of Chrome or Firefox).

## What you'll receive

Before the event we'll invite you into our Slack workspace and the private channel for your course. We'll share links, useful pre-reading, and more. This'll be a place you can ask questions during the course and post achievements afterwards.

On the day of the course, you'll be given access to our online materials and any lab environments, meaning you can spend more time learning and less time solving computer woes.

After the training, you'll have access to our online materials FOREVER. We'll also let you share the link inside your organisation. You'll also be able to access the lab environments for a week after the training so you can recap when you get back to work.
