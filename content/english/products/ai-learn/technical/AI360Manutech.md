---
title : "AI360 ManufacturingTech"
lastmod : "2018-02-17"
draft: true
---

This is the second day of the AI360 Manufacturing Masterclass. The first day looks at developing an AI strategy that aligns with overall business objectives. This second day is for technical practitioners who need to learn more about how to go about building ai systems for manufacturing processes.  

The course will covers areas like how to optimise flow through the manufacturing process, identifying quality issues, and minimising downtime for machinery. It also looks at where businesses can consolidate existing data and what off-the-shelf AI solutions are available.

## On completion of this course you will understand

- The fundamentals of Artificial Intelligence (AI) and Information Architecture (IA).
- Learn about the core applications for AI in manufacturing (Predictive maintenance, Digital Twins, Generative design and Computer vision).
- Develop models based on your business needs - Quick wins for processes
Consolidate existing data and off the shelf solutions.
- Responsible AI (Ethics, Compliance, Regulation).

This course will look at techniques used to improve overall functioning in manufacturing such as quality control,reducing operational costs, and relaying data to give smart insights. It will help understand how to move beyond proof of concept stage to scale AI in order to reap the true benefits.

## Who should attend

You are a technical practiticioner working for a manufacturing company who needs to understand how to adopt AI.  You know that AI could become a key differentiator in manufacturing processes, and you need to stay ahead of the competition.

According to the 2019 Annual Manufacturing Report by The Manufacturer, roughly half of manufacturers have no plans for AI or are unsure to implement it. This leaves the other half actively investing in AI and other digital technologies.  

Those who are getting Artificial Intelligence and Machine Learning right will start getting a 4-10% EBITDA increases from predictive maintenance AI solutions alone. This masterclass is for you if you aim to succeed with AI adoption and leading an AI Ready organisation.  

## Course details

Time:  1 day masterclass

Location:  Face to Face delivery

Cost: This training is for management, leadership and technical teams in manufacturing

Contact: Ruth Kearney (ruth@nightingalehq.ai)
