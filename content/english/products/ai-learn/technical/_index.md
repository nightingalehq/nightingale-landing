---
title: "Technical"
date: 2020-03-02T12:52:36+06:00
image_webp: images/products/thisisengineering-raeng-64YrPKiguAE-unsplash.webp
image: images/products/thisisengineering-raeng-64YrPKiguAE-unsplash-500pxw.jpg
author: Ruth Kearney
description : "AI 360 For Managers who want to win with AI"
---

From AI to BI we teach a wide range of data science skills to tech teams. Learn from experienced industry practitioners. 

> This product is in development. Join our beta programme to get early access to our training platform. [Contact us](../../contact)
