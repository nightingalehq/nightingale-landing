---
title : "Big Data for ML Server"
lastmod : "2018-02-17"
draft: true
---

Learn how to scale your ML when you're facing memory limitations, parallelisation complexities, big data, and operationalising your code.

## Analysing big data with Microsoft ML Server

Microsoft ML Server, a standalone or integrated platform that delivers enterprise scale machine learning capabilities, is a great tool for scaling your ML when you're facing memory limitations, parallelisation complexities, big data, and operationalising your code. This training course takes you through the capabilities for doing analysis and machine learning primarily in the R language.

## Learning Outcomes

- Overview of the Microsoft data science stack.
- Overview of the Microsft ML Server and getting started.
- Working with out of memory data locally.
- Connecting to external ML Servers.
- Leveraging Spark for ML.
- Working with SQL Server.
- The data science workflow in Microsoft ML.
- Deploying models to SQL Server and APIs.

This training broadly aligns with the 70-773 Analyzing big data with Microsoft R exam but has a bit more of an emphasis on common requirements and ease of use, versus some of the breadth the exam will expect.

## What our training days look like

We pair up teaching you important theory with practical exercises to get you confident in your use of the conceptual frameworks and tools you'll be learning. You'll work with real datasets so you'll always be able to see how you can apply things in real life.

## What you'll need on the day

You'll need a device with internet and RDP allowed and ideally a camera. You'll be doing exercises so that you get familiar with the code and the concepts, as well as viewing the online course handouts, and accessing the training group's Slack channel. We use online coding environments so all you need to have installed is a web browser (for best results this should be a recent version of Chrome or Firefox).

## What you'll receive

Before the event we'll invite you into our Slack workspace and the private channel for your course. We'll share links, useful pre-reading, and more. This'll be a place you can ask questions during the course and post achievements afterwards.

On the day of the course, you'll be given access to our online materials and any lab environments, meaning you can spend more time learning and less time solving computer woes.

After the training, you'll have access to our online materials FOREVER. We'll also let you share the link inside your organisation. You'll also be able to access the lab environments for a week after the training so you can recap when you get back to work.
