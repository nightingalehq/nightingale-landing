---
title : "Advanced Excel"
lastmod : "2018-02-17"
draft: true
---

Excel and Google Sheets run the world so helping your people to stay on top of their game and maximising these tools to their full potential is a great way to help your business become more productive and insightful.

## Learning Outcomes

Learn the core fundamentals of how to construct Excel based solutions that are easier to maintain and fix. This helps people develop new solutions better and ensures everyone continues with a good standard of minimum knowledge.

- Work with data from different sources, including how to clean it up and how to combine multiple datasets.
- Streamline multiple datasheets.
- Learn about data visualisation and structuring spreadsheets to support a data entry layer and reporting.  

## What our training days look like

We pair up teaching you important theory with practical exercises to get you confident in your use of the conceptual frameworks and tools you'll be learning. You'll work with real datasets so you'll always be able to see how you can apply things in real life.

## What you'll need on the day

You'll need a device with internet and RDP allowed and ideally a camera. You'll be doing exercises so that you get familiar with the code and the concepts, as well as viewing the online course handouts, and accessing the training group's Slack channel. We use online coding environments so all you need to have installed is a web browser and the ability to RDP.

## What you'll receive

Before the event we'll invite you into the Slack workspace and the private channel for your course. We'll share links, useful pre-reading, and more. This'll be a place you can ask questions during the course and post achievements afterwards.

On the day of the course, you'll be given access to our online materials and any lab environments, meaning you can spend more time learning and less time solving computer woes.

After the training, you'll have access to our online materials FOREVER. We'll also let you share the link inside your organisation. You'll also be able to access the lab environments for a week after the training so you can recap when you get back to work.
