---
title: AI Learn
date: 2020-03-02T06:52:36.000Z
image_webp: images/products/beatriz-perez-moya-XN4T2PVUUgk-unsplash.webp
image: images/products/beatriz-perez-moya-XN4T2PVUUgk-unsplash-500pxw.jpg
author: Ruth Kearney
availability: "PreSale"
description: Build your AI skills and competencies
menu:
  products:
    weight: 1
  main:
    parent: products
---
We offer a wide range of courses to help build core AI skills and competencies in-house and achieve longer-term sustainability. Choose from online, in-person or on-demand formats. Successful AI adopters increase the overall level of data literacy across the organisation. Our courses are designed to accelerate the most in-demand data skills. Our aim is to help you drive collaboration across function relating to data quality and data sharing in order to optimise business results and develop a more inclusive AI-ready culture.  

## Our Approach

### Expert-led

Our courses are taught by experienced practitioners who are experts in their discipline and have a passion for teaching.  We offer one-to-one mentoring to help individuals and teams accelerate their careers in data and have a much bigger impact.  

### Designed to scale

We are experienced at delivering training online and on-demand. This allows companies to build in-house competencies at scale where teams can logon from anywhere in the world and take a course, track progress and access our knowledge library.

### Action Learning

All our courses are driven by an action learning approach. We use a combination of hands on workshops, labs, sprints and project work to reinforce learning. 

### Course Formats

We offer...

* Online remote courses where you can logon on from anywhere in the world
* On-demand where you can watch anytime you like
* In person classes.
* A blend of all of the above.