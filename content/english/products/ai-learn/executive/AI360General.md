---
title : "AI360 General"
lastmod : "2018-02-17"
draft: true
---

This 1-day masterclass takes participants on a deep-dive into the fundamentals of AI, starting with a brief history and building up to the present day, and why this disruptive technology is so vital to future success in business.

## On completion of this course you will

- Understand the fundamentals of Artificial Intelligence (AI) and Information Architecture (IA)
- Recognise how AI complements and strengthens your team
- Learn about the core applications for AI in business
- Develop Quick wins of AI and how to automate manual tasks
- Recognise the growth & revenue opportunities that AI brings 
- Understand Responsible AI (Ethics, Compliance, Regulation)
- Draft your own AI Strategy and AI Canvas.

Key takeaways will include a draft AI strategy, and an understanding of what ‘Quick Win’ AI projects can be developed to save you money, and help your projects gain momentum.

## Who should attend

You are a manager who needs to understand how AI is going to impact your business. You know that AI integration will cover every industry in the next few years, and you need to stay ahead of the competition.

Research shows that 90% of business leaders are planning to implement some form of AI in their organisation; however, 65% of executives who have already done so are reporting failure!

Those who are getting Artificial Intelligence and Machine Learning right, plan to increase use companywide and reap the benefits from doing so. This masterclass is for you if you aim to succeed with AI adoption and leading an ai-ready organisation.

## Course details

Time:  1 day masterclass

Location:  Face to Face delivery

Cost: This training is for management and leadership teams

Contact: Ruth Kearney (ruth@nightingalehq.ai)
