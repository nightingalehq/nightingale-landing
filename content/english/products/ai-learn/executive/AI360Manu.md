---
title : "AI360 Manufacturing"
lastmod : "2018-02-17"
draft: true
---

Whether it is about optimising processes, predicting maintenance or enforcing quality control, AI systems can bring significant cost saving and growth opportunities to your manufacturing business.This 1-day masterclass takes business leaders on a deepdive into how AI is impacting the industry and how to start developing an AI strategy today.  

You will leave with a 360 view of AI, with essential topics relevant to business leaders including; AI Strategy, Information Architecture (IA), AI alignment with Business Objectives, AI-ready culture, AI maturity models, AI Resourcing, Responsible AI, Compliance, AI applications, and Data Readiness

## On completion of this course you will understand

- The fundamentals of Artificial Intelligence (AI) and Information Architecture (IA)
- Building AI Competencies across manufacturing
- Core applications for AI in manufacturing
- AI systems for processes – quick wins
- How to recognise the growth & revenue opportunities that AI brings
- Responsible AI (Ethics, Compliance, Regulation)
- De-risking AI
- How to draft your own AI Strategy and AI Canvas.
- AI strategy and business alignment

Key takeaways will include a draft AI strategy, and an understanding of what ‘Quick Win’ AI projects can be developed to save you money, and help your projects gain momentum.

## Who should attend

You are a leader who needs to understand how AI is going to impact your business. You know that AI could become a key differentiator in manufacturing processes, and you need to stay ahead of the competition. 

According to the 2019 Annual Manufacturing Report by The Manufacturer, roughly half of manufacturers have no plans for AI or are unsure to implement it. This leaves the other half actively investing in AI and other digital technologies.  

Those who are getting Artificial Intelligence and Machine Learning right will start getting a 4-10% EBITDA increases from predictive maintenance AI solutions alone. This masterclass is for you if you aim to succeed with AI adoption and leading an AI Ready organisation.  

## Course details

Time:  1 day masterclass

Location:  Face to Face delivery

Cost: This training is for management and leadership teams in manufacturing

Contact: Ruth Kearney (ruth@nightingalehq.ai)
