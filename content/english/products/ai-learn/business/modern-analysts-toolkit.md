---
title : "The Modern Analyst's Toolkit"
lastmod : "2018-02-17"
draft: true
---

If you want to stay current, learn new skills, and boost your productivity then this four-day course is for you. The intensive and practical training will give you a thorough grounding in the Modern Analyst's Toolkit. You will learn about

- The latest Excel best practices and skills
- SQL and database key concepts
- R for data wrangling, data visualisation, and reporting
- Python for data wrangling and data visualisation

Each day of training will cover one of these four areas, so you have a solid foundation that you can use in your organisation.

The modular format of the course also means that you don't need to attend all the days if you already have strong skills in certain areas. You can simply pick the days that interest you.

The training will be delivered by Steph Locke and Andrew Collier – two fantastic trainers and data scientists.

**Need to convince a manager? Give them our [Overview sheet](../../files/modern-analysts-toolkit-overview.pdf) for the training!**

## What the training days look like

Each day will be structured to cover a technology starting with the basic principles and getting more advanced throughout the day. The rough daily layout will be:

- Important background and inspiring use cases
- Getting started and useful resources
- Diving into topics and supporting exercises
- An extended exercise to put things into practice

## What you'll need on the day

You'll need a laptop. You'll be doing exercises so that you get familiar with the code and the concepts, as well as viewing the online course handouts, and accessing the training group's Slack channel. We use online coding environments so all you need to have installed is a web browser (for best results this should be a recent version of Chrome or Firefox).

If you want to do everything on your machine, we'll upload some high-level install docs to the Slack channel before the event. Note that as we want you and the others on the training to get the most out of each day, if you encounter problems due to using your device to run things on, we'll ask you to switch to the online environments and will help you troubleshoot during breaks.

## What you'll receive

Before the event we'll invite you into the Locke Data Slack workspace and the private channel for your course. We'll share links, useful pre-reading, and more. This'll be a place you can ask questions during the course and post achievements afterwards.

On the first day of the course, you'll be given access to our online materials and our lab environments, meaning you can spend more time learning and less time solving computer woes.

Each day you'll get refreshments and lunch to keep you chipper and able to learn.

After the training, you'll have access to our online materials FOREVER. We'll also let you share the link inside your organisation. You'll also be able to access the lab environments for a week after the training so you can recap when you get back to work.

If you buy the library add-on, we'll ship you a pack of great books that will help you go deeper into what we've covered and extend your knowledge further.

## The syllabus

### Excel

Day One focuses on modern Excel (think 2013+!) and best practices for storing, analysing, and presenting data in Excel.

- Data storage best practices using Tables
- Connecting to different data sources in the Data Model area
- Extending data with calculated fields and custom aggregations
- Building pivot tables and charts across linked data
- Building quality checks and adding tests to your reports

### SQL

>Day Two is all about getting data out of databases using SQL. This will allow you to easily query the databases within your organisation and a number of online public databases. SQL is a truly vital skill!

- Navigating a database
- Working with data types
- Filtering rows and columns
- Building summaries
- Merging data from multiple tables

### R

Day Three is all about R. R is useful on its own, it can be used in-database, and lots of systems like Power BI and Tableau allow you to integrate R code into reports, so it's a super useful language to know.

- Getting data in and out of R
- Working with data types
- Introduction to the tidyverse
- Filtering rows and columns
- Building summaries
- Merging data from multiple tables
- Building charts
- Making reports
