---
title: "Products"
date: 2020-03-02T12:52:36+06:00
menu: 
  main:
    weight: 2
    identifier: products
image_webp: images/blog/blog-post-3.webp
image: images/blog/blog-post-3.jpg
author: Ruth Kearney
description : "Accelerate your AI projects"
---

Our platform consists of three core products;  

- AI Learn offers online and on-demand courses and mentoring programmes to help build core competencies
- AI Connect uses our AI algorithm to match you to the right expertise and skills to help deliver AI projects
- AI Direct is our leadership toolkit that supports managers on the road to change with AI. We offer a set of practical tools and workshops for individual managers and teams to accelerate their strategic efforts.

 
