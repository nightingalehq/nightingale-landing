---
title: App Help
image: /images/uploads/nhq-logo.png
description: Our Knowledgebase contains articles to help you use and navigate our platform.
---

Our Knowledgebase contains articles to help you use and navigate our platform.

See also [help for organisations](https://nightingalehq.ai/tags/help-for-organisations) and [help for partners](https://nightingalehq.ai/tags/help-for-partners).
