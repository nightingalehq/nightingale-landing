---
title: Tech stacks
image: /images/uploads/tech-stacks.jpg
description: A tech stack is a list of programming languages, libraries, frameworks and tools used by developers to create an application.
---

A tech stack is a list of programming languages, libraries, frameworks and tools used by developers to create an application.  

You may also hear tech stacks referred to as data ecosystems or a solutions stack. They are relevant because they tell the developer what the strengths and weaknesses are of an application and indicate what tools and knowledge are necessary to be able to work with it. Correctly defining the tech stacks your company uses or will be using is crucial to finding a good match who will be familiar with your systems.
