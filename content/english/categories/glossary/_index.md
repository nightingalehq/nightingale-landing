---
title: Glossary
image: /images/uploads/glossary.jpg
description: Our Knowledgebase glossary is filled with definitions and details about data science and AI terms and technologies.
---

Our Knowledgebase glossary is filled with definitions and details about data science and AI terms and technologies.
