---
title: Table dev
expires: 2020-05-14
date: 2020-04-15
---
|   Course overview     | Course component                                         |
|-----------------------|----------------------------------------------------------|
| 3-4 week before start | Read and discuss core data, articles in class and online |
| Week 1                | What is AI and how does it add value to business         |
| Week 2                | Data Infrastructure and the Customer Service Process     |
| Week 3                | Automating manual tasks and delivering improvements      |
| Week 4                | Data Visualisation as an intelligence tool               |
| Week 5                | AI tools/processes for growth & revenue opportunities    |
| Week 6                | Data Visualisation as an intelligence tool               |
| Week 7                | AI and my role                                           |
