---
title: Steph Locke
weight: 1
image: images/team/stephlocke.jpg
role: CEO
social:
  - icon: ti-linkedin
    link: https://uk.linkedin.com/in/stephanielocke
  - icon: ti-twitter-alt
    link: https://twitter.com/theStephLocke
  - icon: ti-github
    link: https://github.com/stephlocke
---
Data Scientist, author of several books on R and data science, and now Entrepreneur; Steph is passionate about helping business use their data more effectively, and adopt AI.  After founding Locke Data, a data science consultancy in 2017, she co-founded Nightingale HQ to help businesses adopt artificial intelligence at scale.

She has been an inspiration to many, spreading her passion for all things data by delivering numerous talks and workshops at conferences, local meetups, webinars and online courses. She has also written two data science books to share her knowledge and contributed to others, set up sites and forums to foster and serve tech communities, and organised and hosted events from R user groups to her very own Data Opticon. 

Steph has been recognised for these contributions to data science communities by Microsoft who awarded her two MVPs (Most Valued Professional) in both Data Platform and Artificial Intelligence. She is one of only 3 people in the world to hold this combination.