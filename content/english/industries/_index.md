---
title: "Industries"
seo:
  title: "Industries"
  description: ""
date: 2019-04-09T17:01:46Z
draft: false
menu: "meta"
weight: 1
logo-variant: "white"
---


Artificial intelligence can be applied in many unique and interesting ways, which get more specific when you look at each industry. Take a deeper dive into the workings of AI in your sector.