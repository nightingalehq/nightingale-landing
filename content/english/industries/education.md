---
title: "Education"
subtitle: "AI has the power to transform eductation with tailored and accessible lessons"
seo:
  title: "Opportunities of AI in EdTech"
  description: "AI has the power to transform eductation with tailored and accessible lessons and more engaging content."
date: 2020-01-31T12:06:00
weight: 6
draft: false
icon: "/img/industry/education.jpg"
extract: "Educational technology that uses AI can bed used to enhance educational programmes, tune into students progress and create tailored and accessible lessons."
layout: "wrapped"
logo-variant: "white"
---


AI-powered Educational Technology (EdTech) can help broaden the horizons of education to be more inclusive and increase participation by supporting students and increasing engagement. 

Some institutions have digital assistants that can help students locate classes, answer queries, put them in contact with a lecturer or assist with applications using natural language processing. They may also measure attendance and required grades, sending reminders to keep on track.

AI can be used for personalised learning and assessments, using machine learning algorithms to test a student at their level of knowledge, adjusting the questions if the student is finding it too easy or too hard.

AI can also ease administration for institutions by planning and optimising timetables, helping with the admission of new students, or giving teachers insights on how their pupils are performing and who needs the most attention. 

For planning your AI strategy, deploying multiple AI projects, or getting AI training to boost capabilities within your team, speak to one of our team to see how we can help.
