---
title: What is data platform auditing?
date: 2020-04-08T17:54:18.553Z
author: Mia Hatton
description: >-
  Data platform auditing reviews your data platform against fitness for purpose measures such as security, compliance, and structure.
tags:
  - data platform
  - data security 
  - data collection
  - data storage
  - data analysis
categories:
  - glossary
  - offerings
image: /images/uploads/data-platform-auditing.jpg
executive: >-
  Your organisation's data platform should be a central hub that is secure, efficient and flexible enough to suit your organisation's growing and changing needs. Data platform auditing is needed to ensure that your data platform meets these needs and is set up correctly so that your teams can use it to meet strategic goals.   

  Data platform auditing helps businesses:  


  * ensure compliance and protect assets and data, by reviewing and improving the security of your data platform.

  * reduce costs by reviewing and improving the structure of your data platform.

departmenthead: >-
  Data platform auditing helps teams reduce instances of data loss, data corruption, data inaccuracies and data security on their data platform. Improvements can be made to the design of the data platform to ensure that data storage and structure is consistent and that relationships are clearly defined, which reduces instances of duplication and decreases storage costs. The data platform performance can also be improved to improve the speed of data access. Improving the security of the data platform will ensure compliance and reduce the risk of data loss and corruption.  

  You may need this service if:  


  * your team experiences problems of data duplication, corruption or loss.
  
  * you have a large volume of data on your data platform and do not have security measures in place.

  * your team is using a data platform to gain insights that drive decision-making. 

  KPIs you should consider measuring for this are:  


  * reduced downtime

  * increased speed of data availability

  * costs saved by reducing unnecessary load

  * reduced costs associated with data loss and corruption

technical: >-
  When implementing data science and AI solutions in your products, the availability, security and accuracy of your data are essential. Reviewing and improving your data platform will improve the performance of your projects and make development smoother and more efficient.  

  Data platform auditing helps deliver:  


  * improved data security and compliance


  * faster page loads


  * faster interactive applications


  * user retention


  * reduced bounced rate


  * faster access to data for analysis  


  Get this service if you encounter:  


  * slow performance of web pages and applications.


  * increasing server resource charges.


  * high bounce rates.


  * inadequate data security measures or noncompliance.  


  Key criteria to consider are:  


  * The cost of improvements compared to benefits from those improvements.


  * Data security.


  * GDPR.


  * Any potentially unneeded data.  

---

Welcome to the Nightingale HQ overview of data platform auditing services. Here we aim to introduce people to what they need to know.

## Definition of data platform auditing

Data platform auditing reviews your data platform against fitness for purpose measures such as security, compliance and structure.
