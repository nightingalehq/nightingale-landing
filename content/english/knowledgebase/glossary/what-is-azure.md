---
title: What is Azure?
date: 2020-04-09T14:19:18.038Z
author: Mia Hatton
description: >-
  A cloud computing solution such as Azure provides pay-as-you-go access to vast data storage and computing capabilities and allows you to scale the costs as you grow, avoiding expensive set-up costs.
tags:
  - Azure
  - cloud service
  - ai
  - data storage
  - data analysis
categories:
  - glossary
  - tech stacks
image: /images/uploads/azure.jpg
executive: >- 
  Implementing data science and AI in your business requires vast data storage capabilities and expensive infrastructure. A cloud computing solution such as Microsoft Azure provides pay-as-you-go access to these systems and allows you to scale the costs as you grow, avoiding expensive set-up costs.  

  Microsoft Azure helps businesses:


  - adopt AI without expensive set-up costs

  - develop high-quality applications that collect and leverage data to maximise revenue
departmenthead: >-
  Microsoft Azure helps teams adopt AI solutions that can drive growth and improve performance, as well as providing solutions to host applications and build improved functionality that will drive sales. Microsoft Azure is a cloud computing platform, which allows pay-as-you-go access to storage and computing services without expensive infrastructure set-up fees.  

  Signs your department should invest in this are:  


  - you are developing an application and need a hosting solution that is cheap to implement and will scale as you grow

  - you are planning to adopt AI in your team and need access to data storage and machine learning tools at a price that will scale with your needs

  KPIs you should consider measuring for this are:  


  - improved sales when AI features are implemented

  - savings on infrastructure investment

  - improved efficiency of resource management

  - improved product performance
technical: ''
---
Welcome to the Nightingale HQ overview about Azure services. Here we aim to introduce people to what they need to know.

## Definition of Azure

From [Microsoft](https://azure.microsoft.com/en-gb/overview/what-is-azure):

> Microsoft Azure is an ever-expanding set of cloud services to help your organisation meet your business challenges. It’s the freedom to build, manage and deploy applications on a massive, global network using your favourite tools and frameworks.  

Azure is a [cloud service provider](https://nightingalehq.ai/tags/cloud-service). Other cloud vendors to consider are:  

- [Amazon Web Services (AWS)](https://nightingalehq.ai/knowledgebase/glossary/what-is-aws)
- [IBM Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-ibm-cloud)
- [Google Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud)

## Key technologies

There are more than 600 products available from Microsoft Azure, across 22 categories. These products include:

### AI and machine learning

- **Cognitive Services** are a suite of AI capabilities that can be integrated into your apps through API calls. This allows developers without any AI or machine learning experience to build intelligent apps
- **Machine Learning** services allow developers to build, train and deploy machine learning models. The service supports a range of open source tools and framworks, including PyTorch, TensorFlow, and scikit-learn
- **Azure Bot Service** supports development of intelligent, enterprise-grade bots such as QnA bots and virtual assistants. The service integrates with Cognitive Services to add powerful AI capabilities, including natural language processing (NLP) and computer vision
- **Azure Cognitive Search** is a cloud search service with built-in AI capabilities such as NLP and computer vision. The service makes it easier to work with unstructured data (e.g. text and images) by organising and structuring it to make it searchable
- **Azure Databricks** is a collaborative data science environment based on Apache Spark, that is optimised to work with other Azure services. It provides a platform in which teams can collaborate to gather, analyse and model data. Interaction with data is possible through interactive notebooks that support a range of languages - including Python, R, Scala, and SQL - and deep learning frameworks

### Analytics

- **Azure Stream Analytics** is an intuitive, real-time analytics platform that can handle complex anaytics at high speed. The service is easy to set up using SQL and can be extended with C#, JavaScript, and built-in machine learning capabilities
- **Azure Analysis Service** is a scalable and secure data analytics platform that can combine data from multiple sources to produce shareable insights that integrate with Power BI for visualisation
- **Azure Synapse Analytics** is a highly scalable solution to analyse large volumes of data, especially across distributed data sources like data lakes

### Data storage and management

- **Azure SQL Database**, **Azure Database for PostgreSQL**, **Azure Database for MySQL**, and **Azure Database for MariaDB** are *relational databases* to support analytics or operational activities, saving the need to manage the underlying infrastructure or tasks like backups
- **Azure Cosmos DB** is Microsoft's globally distributed, multi-model database service, which provides highly scalable storage for data that can be accessed using a variety of APIs including SQL, MongoDB, Cassandra, Tables, and Gremlin
- **Table Storage** is a high-latency NoSQL datastore for structured data
- [**Data Factory**](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure-data-factory) is a service that allows you to integrate data from several sources for transformation and analytics
- **Azure Data Lake Storage** is a highly scalable and cost-effective data lake solution for big data analytics. It is a centralised repository for vast quantities of structured and unstructured data

## Security and Compliance

Microsoft are commited to security, privacy and compliance across their Azure products. The [Azure Trust Center](https://www.microsoft.com/en-gb/trust-center/?rtc=1) provides details of Microsoft's foundational principles of trust: security, privacy, compliance and transparency.  

### Security

Microsoft Azure features multi-layered, built-in security controls that can be managed via intuitive controls. Intelligent features helps to identify and protect against potential threats.  Security products from Azure include **Security Center** and **Key Vault**.  
Read more about Azure security [here](https://azure.microsoft.com/en-gb/overview/security/).

### Privacy

The Microsoft Privacy Standard enforces strict protections on Microsoft business processes. Using Azure products, you have a clear view of where your data is stored, how it is secured, and who can access it. You can choose the geographical location of your data centre when you set up your Azure products.  
Read more about Azure data privacy [here](https://azure.microsoft.com/en-gb/overview/trusted-cloud/privacy/).

### Compliance

Microsoft Azure holds more than 90 compliance certifications, and features products and tools that help you to ensure compliance. Tools such as **Compliance Manager** allow you to track your compliance with GDPR and other privacy regulations. **Azure Security and Compliance Blueprints** can be used to create, deploy and update compliant environments for your data.  
Read more about Azure compliance [here](https://azure.microsoft.com/en-gb/overview/trusted-cloud/compliance/).

## Pricing

Microsoft Azure has a competitive, scalable pricing structure with several options to support businesses as they grow. From the dashboard you can access at-a-glance cost overviews to manage spending, and the Azure [pricing calculator](https://azure.microsoft.com/en-gb/pricing/calculator/) helps you to budget and estimate costs of using Azure services.

Creating an [account](https://azure.microsoft.com/en-gb/free/) on Microsoft Azure provides:

- 12 months of free access to select services, including data storage and some AI services
- £150 credit to use on any Azure service for 30 days
- access to more than 25 always-free services, including Security Center and Search

Beyond the free services, Microsoft Azure offers the choice between a pay-as-you-go (PAYG) model and reserved pricing. With either option, you are billed monthly for the services you use or reserve.  

### Pay as you go

A great option for scalability, Azure products are charged by time, events and storage space.  

For example, Azure Machine Learning services are charged by hour of use by the virtual machine, Azure Bot Service is charged per thousand messages, and pricing for Azure SQL database is calculated based on hours of available access and storage space, in incremenets of 32GB.

### Reserved pricing

If you are confident of the computing power and storage space you will need for your application in advance, the reserved pricing model can save you 20-30% of costs. In this model, you reserve the time and storage space required for the next year or two years, and pay for it monthly.

### Azure Dev/Test Pricing

Microsoft Azure offers discounted rates to support ongoing development and testing. The discounts are available for the following services:

- Windows Virtual Machines*
- BizTalk Virtual Machines*
- Azure SQL Database*
- SQL Server Virtual Machines
- Logic Apps Enterprise Connection
- App Service Instances
- Cloud Service Instances
- HD Insight Instances

*These services also remain eligible for reserved instance pricing in addition to dev/test discounts.

You can find out more about Dev/Text Pricing here.

### Purchase Azure services through a Cloud Solution Provider (CSP)

As well as having the option to buy directly from Microsoft Azure, you can go through a CSP who will provision, manage and support your subscriptions. This is a good option if you already purchase Microsoft solutions through a CSP, as they will consolidate your bills and ensure you are not using duplicate services. A CSP provides local support and expertise to help you get started with cloud services and manage costs, and will often offer bundle deals that can save you money.
You can find a CSP [here](https://partner.microsoft.com/en-us/cloud-solution-provider/find-a-provider).

### Support

All Azure accounts come with free customer service, documentation, and tutorials to help you get started. Additional technical support becomes available when you purchase a support plan, starting at just over £20 per month. Learn more about the support plans [here](https://azure.microsoft.com/en-gb/support/plans/).
