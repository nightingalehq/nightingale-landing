---
title: What are long-range, low-power sensor networks?
date: 2020-04-08T14:50:01.368Z
author: Mia Hatton
description: >-
  Long-range, low-power sensor networks (also called Long Range Wide Area Network or LoRaWan) help businesses develop smart, IoT products or integrate connectivity into existing products.
tags:
  - data collection
  - iot
  - product development
categories:
  - glossary
  - tech stacks
image: /images/uploads/sensor-network.jpg
executive: >-
  Long-range, low-power sensor networks can be deployed over large areas to collect data that can be analysed and developed into predictive models to drive strategic decisions. They can also be integrated into existing products to add intelligent features that will make them more appealing to users and to gather usage data to improve their performance over time.

  Long-range, low-power sensor networks help businesses:

  - develop smart, IoT products or integrate connectivity into existing products.
  - gather data across wide areas that can be used to develop better products and make better decisions through [data science](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-science).

departmenthead: >-
  Long-range, low-power sensor networks help teams to develop smart products, both by allowing the products to connect to other IoT technologies and by gathering data to make better decisions. If your business makes use of monitoring devices, implementing long-range, low-power sensor networks will improve the efficiency and accuracy of the devices' data collection and reduce downtime. Monitoring devices are used in agriculture, for example, to assess yield.  

  You may need this service if:  

  - your industry makes use of monitoring devices.

  - you are developing products that would be improved by connectivity.  


  KPIs you should consider measuring for this are:  


  - Lower downtime.

  - Improved products measured by increased sales and retention.

technical: >-
  Long-range, low-power sensor networks use narrow bandwidth transmissions to achieve long-range. They can be implemented at relatively low cost due to low-power (thanks to limited daily server communications) and use of unlicensed spectrum. The fact that the sensor networks are low-power also means that battery life can be ten years or more, saving resources.  

  Long-range, low-power sensor networks help deliver:  

  - efficient and accurate usage data for your product.

  - improved performance.

  - connectivity over long distances and on unlicensed spectrum.  


  Get this service if you encounter:  

  - high power usage.

  - issues with network range for your product or monitoring devices.

  - a high number of network gateways.

  - high costs of using licensed spectrum.  


  Key criteria to consider are:  

  - How much data do you need to transmit and how frequently? 

  - Where will your devices be located?

  - Will you need to add your own network/Gateways?

  - What are your target costs for setting up and maintaining the network?

  - How much autonomy do you require over your network?  


---

Welcome to the Nightingale HQ overview of long-range, low-power sensor networks services. Here we aim to introduce people to what they need to know.

## Definition of long-range, low-power sensor networks

Long-range, low-power sensor networks are a type of wireless technology. They are systems of connected sensors that work over a large area using little power. This means that they have the benefits of wide-area mobile networks at a lower cost. They do not allow for large data transfers, so are not used for human communications (voice, video, and other communications that transfer large volumes of data). However, their capacity to connect devices across wide distances makes them useful for Internet of Things (IoT) technologies and other machine-to-machine communications.
