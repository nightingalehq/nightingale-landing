---
title: What is Google Cloud BigQuery?
date: 2020-04-09T14:58:18.553Z
author: Mia Hatton
description: >-
  BigQuery is a fully-managed, serverless cloud data warehouse that scales with your needs and is easy to set up and manage.
tags:
  - ai
  - data storage
  - data analysis
  - data warehouse
  - cloud service
  - google cloud
categories:
  - glossary
  - tech stacks
image: /images/uploads/google-bigquery.png
executive: ''
departmenthead: ''
technical: ''
---
Welcome to the Nightingale HQ overview about BigQuery.
Here we aim to introduce people to what they need to know.

BigQuery is a Google Cloud product. Read more about Google Cloud [here](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud/).

## Definition of BigQuery

From [Google](https://cloud.google.com/bigquery/what-is-bigquery):  

> Storing and querying massive datasets can be time consuming and expensive without the right hardware and infrastructure. BigQuery is an enterprise data warehouse that solves this problem by enabling super-fast SQL queries using the processing power of Google's infrastructure. Simply move your data into BigQuery and let us handle the hard work. You can control access to both the project and your data based on your business needs, such as giving others the ability to view or query your data.

Check out our article '[What is Data Warehousing?](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-warehousing/)' to find out more the benefits of a data warehouse.

## Does you organisation need BigQuery?  

BigQuery is a fully-managed, serverless cloud data warehouse that scales with your needs and is easy to set-up and manage. By adopting BigQuery, you can avoid the hassle of developing and managing the infrastructure needed to store large, complex datasets, allowing you to focus on gaining insight from your data. You can query your data using standard SQL statements, making it easy to get started with analysing your data.

You may need BigQuery if:

- Your organisation holds large volumes of data across disparate sources
- Querying your data takes a long time because it is stored or processed inefficiently
- You want to avoid the cost implications of setting up a data warehouse (e.g. hardware purchase)
- You want to implement [Business Intelligence](https://nightingalehq.ai/knowledgebase/glossary/what-is-business-intelligence/) quickly and easily

## Benefits

Benefits of BigQuery include:

- You can run queries fast enough to analyse terrabytes of data in [seconds](https://www.youtube.com/watch?v=eyBK9nj-7AA&autoplay=1)
- It scales automatically with your usage
- It replicates and deploys your data across multiple data centres to maximise availability
- You can reveal real-time insights from streaming data
- BigQueryML allows data scientists to implement machine learning mdels within BigQuery
- BigQuery GIS provides support for geospatial analysis, "making BigQuery the [only cloud data warehouse](https://cloud.google.com/bigquery) with built-in GIS functionality"

## Technical considerations

### Prerequisites and Integrations

To get started with BigQuery, you need a Google Cloud account. Read more about Google Cloud [here](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud/).  

Google Cloud offers comprehensive [documentation](https://cloud.google.com/bigquery/#migrate-teradata) to assist you if you plan to migrate your data from a different Data Warehouse environment, such as Teradata of [Amazon RedShift](https://knowledge.nightingalehq.ai/redshift).  

BigQuery integrates with a suite of Google Cloud [tools and partners](https://cloud.google.com/bigquery#partners), including:  

#### Data Integration Solutions

- Informatica
- SAP
- Confluent
- SnapLogic

#### BI and data visualisation

- Tableau
- Looker
- Qlik

### Security and Compliance  

Google Cloud services are built on the same secure-by-design infrastructure that is used by Google itself. Google has a policy of creating trust through transparency so you can read all about their security, privacy and compliance measures by visiting their [Trust and Security centre](https://cloud.google.com/security/).

BigQuery provides strong security and governance controls with fine-grained Identity and Access Management, and your data is always encrypted at rest and in transit.

### Pricing

BigQuery is a pay-as-you-go service so you only pay for what you use, and there are no up-front set-up fees. Read more about Google Cloud and its pricing structure [here](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud).  

The cost of using BigQuery depends on both your storage and query requirements, as outlined below:

Service | Price
--- | ---
Storage | $0.02 per GB, per month
Storage (long term) | $0.01 per GB, per month
Streaming data | $0.01 per 200 MB
Querying data (fist 1 TB per month) | Free
Querying data (beyond 1 TB per month) | $5 per TB

Flat-rate pricing is also available for querying data, starting at $10000 per month for reserving 500 slots.

You can read more about BigQuery pricing [here](https://cloud.google.com/bigquery/pricing).  

## Alternatives to BigQuery

If you already use Google Cloud services then BigQuery is a sensible option for your data warehouse. Other cloud vendors offer their own data warehouses, which you should also consider. When choosing a data warehouse service, considee the cost of storing and querying data, the speed of data analysis, the availability and security of your data, and the integrations available.

Other cloud data warehousing services include:

- [Redshift](https://aws.amazon.com/redshift) from [AWS](https://nightingalehq.ai/knowledgebase/glossary/what-is-aws)
- [IBM Db2 Warehouse on Cloud](https://www.ibm.com/uk-en/analytics/data-warehouse#2109616) from [IBM Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-ibm-cloud)
- [Azure Synapse](https://azure.microsoft.com/en-gb/services/synapse-analytics/) from [Azure](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure)
