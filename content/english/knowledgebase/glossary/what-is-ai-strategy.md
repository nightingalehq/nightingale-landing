---
title: What is AI strategy?
date: 2020-04-09T08:53:18.553Z
author: Mia Hatton
description: >-
  An artificial intelligence strategy should be closely linked to your data strategy and hence to your business objectives. It provides a roadmap for executing your business's AI priorities.
tags:
  - data science
  - data culture
  - strategy
  - training
  - AI
categories:
  - glossary
  - offerings
image: /images/uploads/ai-strategy.jpg
executive: >-
  Setting up an AI strategy helps businesses:  

  - to diagnose which business goals can be solved with AI.

  - adjust the culture and mindset in the business to be accepting and ready for AI.

  - set clear, achievable plans and goals for their AI initiatives.

departmenthead: >-
  AI strategy helps align teams towards a common goal and ensures the right foundation and culture have been laid for success.

  You may need this service if:  


  - your [data strategy](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-strategy) doesn't have a clear section dedicated to AI for improving data quality and data governance.

  - you have knowledge or skills gaps in your team that need addressing.

  - you have identified an AI use case but do not know how or have been unable to execute it.

  If you want to measure the performance of your AI strategy, you should set KPIs that are in line with your data and business strategy and associated KPIs. Your AI strategy should also help you achieve your business priorities.

technical: >-
  The AI strategy of your organisation will identify opportunities for automation, AI product development and AI integration into products. In order to successfully follow the strategy to meet business goals, technical teams need to lead the way in data culture and ensure that data is appropriately collected and stored to drive the AI initiatives.   

  AI strategy helps deliver:  


  - increased automation.

  - more successful AI initiatives.

  - increased adoption of AI in product development.

  - a more positive attitude towards data and AI from the whole organisation.

  
  Get this service if you encounter:  

  - a lack of enthusiasm for AI initiatives.

  - uncertainties as to which potential AI initiatives are aligned to business goals.

  - difficulties deploying AI products.

  
  Key criteria to consider are:  

  - What tech stacks will be required to deliver AI initiatives?

  - Are there skill gaps in your team that need to be filled in order to follow the AI strategy?

  - Will you need to make changes to the way that your data is stored and accessed in order to deliver AI initiatives?

---

Welcome to the Nightingale HQ overview of AI strategy services. Here we aim to introduce people to what they need to know.  

## Definition of AI strategy

AI strategy refers to a company's vision for how AI will be deployed to help achieve the company's business goals. It should be closely linked to your [data strategy](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-strategy) and hence to your business objectives. It provides a roadmap for executing your business's AI priorities.  
