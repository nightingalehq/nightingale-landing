---
title: What are Azure Cognitive Services?
date: 2020-04-09T12:51:18.038Z
author: Mia Hatton
description: >-
  Azure Cognitive Services is a collection of APIs and services that you can use to integrate intelligent features into your applications. The services are grouped into five categories: decision, language, speech, text, vision, and search.
tags:
  - azure
  - ai
  - product development
  - cloud service
  - cognitive services
categories:
  - glossary
  - tech stacks
image: /images/uploads/cognitive-services.jpg
executive: ''
departmenthead: ''
technical: ''
---

Welcome to the Nightingale HQ overview about Azure Cognitive Services.
Here we aim to introduce people to what they need to know.

Azure Cognitive Services is a Microsoft Azure product. Read more about Microsoft Azure [here](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure).

## Definition of Azure Cognitive Services

From [Microsoft](https://azure.microsoft.com/en-gb/services/cognitive-services/):  

> Azure Cognitive Services: A comprehensive family of AI services and cognitive APIs to help you build intelligent apps.
> Cognitive Services bring AI within reach of every developer – without requiring machine-learning expertise. All it takes is an API call to embed the ability to see, hear, speak, search, understand and accelerate decision-making into your apps.

Cognitive Services are divided into five categories as outlined below.

Category | Description | Example services
--- | --- | --- 
Decision | Decision services empower you to "make smarter decisions faster". This includes offering customers personalised content and detecting and acting on potential problems or problematic content. | Content Moderator, Personaliser
Language | Language services extract meaning from text, making it easier to support your customers from within your application. | QnA Maker, Text Analytics, Translator Text
Speech | Speech services perform speech processing, including translation and converting between speech and text. | Speech-to-Text, Text to Speech, Speech translation
Vision | Vision services provide capability to process images and videos, and analyse the data they extract. | Computer Vision, Face, Ink Recogniser
Web search | Web search services integrate Bing search capabilities into your application. | Bing Spell Check, Bing Autosuggest, Bing Visual Search

## Does you organisation need Azure Cognitive Services?  

Azure Cognitive Services comprise a collection of APIs (application program interfaces), SDKs (software development kits), and services that developers can use to make their apps more intelligent without the need to build and train algorithms from scratch.  

You may need Azure Cognitive Services if:

- You want to improve your app's performance with intelligent features but do not have the resources to build them
- You want to gain insights from your data such as sentiment analysis and video content, but lack the skill to build and train models
- Your app requires intensive administration, for example content moderation or customer service, and you want to reduce the burden

## Benefits

Benefits of Azure Cognitive Services include:

- The services provide pre-trained intelligent algorithms, saving the need to invest in building and training models from scratch
- They are quick to set up and can be easily integrated into existing applications
- They add value to your application by improving user experience, and reduce your administrative load
- All of the services offer a free trial tier so you can experiment with them before committing
- Beyond the free trial, the pricing scales with usage so you only pay for the services you use

## Technical considerations

### Prerequisites and Integrations

To get started with Azure Cognitive Services, you need a Microsoft Azure account. Read more about Microsoft Azure [here](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure).  

Setting up an Azure Cognitive Services product is quick and easy, and a free tier is available for all of the different services so you can experiment with the services before spending any money.

To set up a service:

1. Get a free account on [Microsoft Azure](https://azure.microsoft.com/en-gb/free/), if you don't already have one
2. Go to the Azure Marketplace via the tile on your [dashboard](https://portal.azure.com)
3. Search for the service you want to use and click `Create`
4. Give the resource a name and choose your subscription, resource location, and resource group. Select the free pricing tier (F0) to begin with.
5. When the resource is created, save the API key. You will use this to make calls to the resoure from within your application.

Once you have your API key you can call the service from another application, for example:

- [Azure Notebooks](https://notebooks.azure.com/), or any other Jupyter Notebook
- Docker containers (for select [services](https://docs.microsoft.com/en-us/azure/cognitive-services/cognitive-services-container-support))

You can also integrate the services into your own application by calling the API.

You can monitor your usage using the Azure Dashboard.

### Security and Compliance  

Azure Cognitive Services are built on Microsoft Azure security infrastructure and use all Microsoft Azure security measures. You can read more about Microsoft Azure security [here](https://www.microsoft.com/en-gb/trust-center/?rtc=1).  

The privacy of data processed by Azure Cognitive Services is maintained in alignment with Azure's privacy commitments, which you can read about [here](https://www.microsoft.com/en-us/trust-center/privacy).

Microsoft Azure carries an impressive list of compliance certifications which you can view [here](https://docs.microsoft.com/en-gb/microsoft-365/compliance/offering-home).  

### Pricing

Azure Cognitive Services are pay-as-you-go services so you only pay for what you use, and there are no up-front set-up fees. Read more about Microsoft Azure and its pricing structure [here](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure).  

The pricing structure differs between services. Each service offers a free tier allowance (for example, Bing Autosuggest provides 1000 monthly transactions on the free tier), and beyond the free tier the services are billed by number of transactions (calls to the API) or by amount of data processed. For example:

- Content Moderator is billed per 1000 transactions
- Speech-to-text is billed per audio hour
- Text to Speech is billed per million characters

These billable transactions reduce in cost as monthly thresholds are surpassed.

You can read more about Azure Cognitive Services pricing [here](https://azure.microsoft.com/en-gb/pricing/details/cognitive-services/).  

## Alternatives to Azure Cognitive Services

There are other Machine Learning APIs available from different vendors, however Azure Cognitive Services offers the most extensive collection of services. If a competitor offers a service that interests you, your choice will mostly be determined by preference between the vendors, suitability of the interface, individual features and your existing skills.

Other Machine Learning APIs include:

- Amazon Lex, Amazon Translate, and Amazon Rekognition from [AWS](https://nightingalehq.ai/knowledgebase/glossary/what-is-aws)
- IBM Watson and IBM Visual Recognition from [IBM Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-ibm-cloud)
- Dialogflow, Cloud Speech API and Cloud Vision API from [Google Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud)
