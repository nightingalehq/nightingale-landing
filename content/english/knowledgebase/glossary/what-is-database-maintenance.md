---
title: What is database maintenance?
date: 2020-04-07T17:14:59.218Z
author: Mia Hatton
description: >-
  Database maintenance helps teams to ensure that accurate data is reliably
  available, that their databases are not being overloaded and that their data
  is secure.
tags:
  - data storage
  - data security
categories:
  - glossary
  - offerings
image: /images/uploads/database-maintenance.jpg
executive: >-
  Data is important to achieving your strategic goals and database maintenance
  ensures that your data is stored securely, is accurate, and is readily
  available to appropriate parties.


  Database maintenance helps businesses:


  * reduce downtime and data loss.

  * ensure accurate and reliable data is available for
  [modeling](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-modeling) and
  [insights](https://nightingalehq.ai/knowledgebase/glossary/what-is-business-intelligence).
departmenthead: >-
  Database maintenance helps teams to ensure that accurate data is reliably
  available, that their databases are not being overloaded and that their data
  is secure.


  You may need this service if:


  * your team experience database downtime.

  * you have a large volume of data and do not have maintenance or security
  measures in place.

  * your team is using data to gain insights that drive decision-making.


  KPIs you should consider measuring for this are:


  * reduced downtime

  * increased speed of data availability

  * improved error logging

  * costs saved by reducing unnecessary load

  * reduced costs associated with data loss and corruption
technical: >-
  When implementing data science and AI solutions in your products, the
  availability, security and accuracy of your data are essential. Implementing
  database maintenance will improve the performance of your projects and make
  development smoother and more efficient.


  Database maintenance helps deliver:


  * improved error tracking

  * data and log file management

  * index fragmentation

  * statistics

  * corruption detection

  * backups


  Get this service if you encounter:


  * slow performance and high storage costs due to inefficient data storage or
  data inaccuracies.

  * poor file management.

  * difficulties accessing data.

  * occurrences of data corruption or data loss.


  Key criteria to consider are:


  * most appropriate tech stack

  * data security
---
Welcome to the Nightingale HQ overview of database maintenance services. Here we aim to introduce people to what they need to know.

## Definition of Database maintenance

From [Science Direct:](https://www.sciencedirect.com/topics/computer-science/database-maintenance)

> *Database maintenance plans* are a method of ensuring that a database is optimised and performing well.
