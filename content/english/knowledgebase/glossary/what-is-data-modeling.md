---
title: What is data modeling?
date: 2020-04-07T10:15:15.885Z
author: Mia Hatton
description: >-
  Data modeling helps businesses to build products that use data efficiently and
  meet business goals.
tags:
  - data sources
  - data storage
  - product development
categories:
  - glossary
  - offerings
image: /images/uploads/data-modeling.jpg
executive: >-
  The quality of a business data model reflects the organisation of the whole
  business. A messy data model with inefficient storage and access solutions can
  reflect dysfunctional business processes, and lead to slow, inefficient
  decision making. A data modeling initiative can sit as part of an initiative
  for cultural and process changes.


  Data modeling helps businesses:


  * build products that use data efficiently and meet business goals.

  * empower teams to make data-driven decisions efficiently.

  * manage large volumes of data.
departmenthead: >-
  Data modeling provides a clear, big-picture view of your data sources,
  transfers and relationships. Developing a data model is the first step towards
  ensuring that your products use data efficiently and reducing unnecessary
  server costs, and enables you to make decisions more quickly and efficiently.


  You may need this service if:


  * you are building an application that will access multiple data sources.

  * you manage a large volume of data and want to store and access it
  efficiently.

  * it takes a long time to make data-driven decisions because your data is
  difficult to find and analyse.


  KPIs you should consider measuring for this are:


  * reduced costs of data storage and querying

  * improved efficiency of project delivery

  * improved efficiency of decision making when a big picture view is quickly
  and easily available
technical: >-
  Data modeling helps teams to design systems and software that utilise data,
  and to design effective database structures. Your application will need to
  access and store data efficiently to reduce server costs. Developing a clear
  view of your data sources and relationships will guide your development to
  avoid repetitive queries and manage data effectively.


  Data modeling helps deliver:


  * products that can integrate data from several sources and process it
  effectively.

  * products that store and process data efficiently to reduce load.


  Get this service if you encounter:


  * high expenses from repeated, unnecessary server requests.

  * high expenses from data storage.

  * lags and bugs in your product due to inefficient data processing.


  Key criteria to consider are:


  * Are you able to identify all of your data sources and how data is queried by
  your application?

  * Do you have the time and resources available to audit your data storage and
  processing solutions?
---
Welcome to the Nightingale HQ overview of data modeling services. Here we aim to introduce people to what they need to know.

## Definition of data modeling

From [Wikipedia](https://www.wikipedia.org/wiki/Data_Modeling)

> Data modeling in software engineering is the process of creating a data model for an information system by applying certain formal techniques.
>
> Data modeling is a process used to define and analyze data requirements needed to support the business processes within the scope of corresponding information systems in organizations. Therefore, the process of data modeling involves professional data modelers working closely with business stakeholders, as well as potential users of the information system.

Data modeling is the process of structuring and organising data as a map that clearly visualises data sources, transfers and relationships. It is applied during the design stage of an application or system to understand how data will be used to support it.
