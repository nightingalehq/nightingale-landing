---
title: What is remote support?
date: 2020-04-07T18:34:15.885Z
author: Mia Hatton
description: >-
  Remote support helps businesses to gain the benefits of an in-house data science expert without the commitment or costs of hiring an employee.
tags:
  - data science
  - data science consulting
categories:
  - glossary
  - offerings
image: /images/uploads/remote-support.jpg
executive: >-
  An expert can help your business to meet its strategic goals via remote support. It provides the opportunity to develop unique products and build [data science](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-science) capabilities within your organisation without the commitment of costs of hiring dedicated support teams.  

  Remote support helps businesses:  


  * gain the benefits of an in-house data science expert without the commitment or costs of hiring an employee.

  * remove barriers to adoption by bringing in outside help regardless of location.

departmenthead: >-
  Remote support helps teams to access technical expertise wherever you are located. Through remote support, an expert can help your team to succeed in their data science projects whilst keeping costs low. This is useful if you are planning to launch a specific product that requires expert advice, or if you have a small team that doesn't necessarily need a dedicated IT support team.

  You may need this service if:


  * you are building a data science team and need to bring in an expert to provide direction and support while the team builds their skills and expertise.
  
  * you are undergoing a data or AI project with a fast turnaround and need expert help.
  
  * you want to remove geographical restrictions on bringing in expert help.


  KPIs you should consider measuring for this are:


  * Reduced costs from using remote support consultant compared to in-house staff.

  * Improved efficiency of project delivery.
technical: >-
  Remote support helps to expand the technical skill and expertise of your team by bringing in outside support.

  Remote support helps deliver:


  * increased efficiency of development when help from an expert is deployed.

  * keeping production going while staff are being trained.

  * data support for all teams, freeing up technical staff.


  Get this service if you encounter:


  * Too much time spent supporting other teams with data-related enquiries.

  * You are undergoing a data or AI project with a fast turnaround and need expert help.


  Key criteria to consider are:


  * Does the remote support agent understand your needs?

  * Is the remote support agent familiar with your tech stacks?

---

Welcome to the Nightingale HQ overview of remote support services. Here we aim to introduce people to what they need to know.

## Definition of remote support

From [Wikipedia](https://www.wikipedia.org/wiki/Remote_support)

> In information technology (IT), remote support tools are IT tools and software that enable an IT technician or a support representative to connect to a remote computer from their consoles via the Internet and work directly on the remote system. Although its main focus is the access to computers located anywhere in the world, the remote support applications also provide features like file transfer, desktop sharing, file synchronization, command line or guest accessibility.
