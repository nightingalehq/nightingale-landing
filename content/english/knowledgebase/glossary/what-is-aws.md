---
title: What is AWS (Amazon Web Services)?
date: 2020-04-09T12:50:18.038Z
author: Mia Hatton
description: >-
  A cloud computing solution such as AWS provides pay-as-you-go access to vast data storage and computing capabilities and allows you to scale the costs as you grow, avoiding expensive set-up costs.  
tags:
  - aws
  - cloud service
  - ai
  - data storage
  - data analysis
categories:
  - glossary
  - tech stacks
image: /images/uploads/aws.png
executive: >- 
  Implementing data science and AI in your business requires vast data storage capabilities and expensive infrastructure. A cloud computing solution such as AWS provides pay-as-you-go access to these systems and allows you to scale the costs as you grow, avoiding expensive set-up costs.  

  AWS helps businesses:


  - adopt AI without expensive set-up costs

  - develop high-quality applications that collect and leverage data to maximise revenue
departmenthead: >-
  AWS helps teams adopt AI solutions that can drive growth and improve performance, as well as providing solutions to host applications and build improved functionality that will drive sales. AWS is a cloud computing platform, which allows pay-as-you-go access to storage and computing services without expensive infrastructure set-up fees.  

  Signs your department should invest in this are:  


  - you are developing an application and need a hosting solution that is cheap to implement and will scale as you grow

  - you are planning to adopt AI in your team and need access to data storage and machine learning tools at a price that will scale with your needs

  KPIs you should consider measuring for this are:  


  - improved sales when AI features are implemented

  - savings on infrastructure investment

  - improved efficiency of resource management

  - improved product performance
technical: ''
---
Welcome to the Nightingale HQ overview about AWS services. Here we aim to introduce people to what they need to know.

## Definition of AWS (Amazon Web Services)

From [ideaminetech](https://www.ideaminetech.com/blog/aws-services-in-simple-terms/):

> Amazon Web Services is a cloud computing platform that provides customers with a wide array of cloud services. We can define AWS (Amazon Web Services) as a secured cloud services platform that offers compute power, database storage, content delivery and various other functionalities. To be more specific, it is a large bundle of cloud based services. Consider we need electricity for our home. Either we can generate our own electricity or purchase it from electric power companies. When we generate our own electricity, we need set up a lot of Infrastructure costing us a lot of money. Instead of that, we could purchase electricity and pay as we use. Similarly, AWS is one of the cloud computing providers that provide us computing, storage, networking and lot more services that we can pay as we use.  

AWS is a [cloud service provider](https://nightingalehq.ai/tags/cloud-service). Other cloud vendors to consider are:

- [Microsoft Azure](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure)
- [IBM Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-ibm-cloud)
- [Google Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud)

## Key technologies

There are more than 165 services available from AWS, across 24 categories. These products include the following:

### Data storage and management

- [**Amazon S3**](https://nightingalehq.ai/knowledgebase/what-is-amazon-s3) (Simple Storage Service) is a scalable, secure, and durable object storage service with easy-to-use management tools. Data can be stored as different classes depeneding on frequency of access needs, to keep storage costs low.
- **AWS Backup** is a backup service that allows you to centralise and automate backups of data from across all AWS services.
- **Amazon EFS** (Elastic File System) is a scalable, elastic file storage system that automatically grows and shrinks to match demand. Data can be stored as an Infrequent Access class to save costs on storage on data that is not accessed every day.
- **Amazon Aurora** and **Amazon RDS** (Relational Database Service) are relational databases that support analytics and operational activities, saving the need to manage the underlying infrastructure or tasks like backups.
- **Amazon Neptune** is a fast, fully managed graph database service that supports both Property Graph and W3C's RDF graph models. It can be used to store billions of relationships to support applications in use cases such as recommendation engines and network security.
- **Amazon DynabmoDB** is a serverless, scalable key-value and document database service.
- **AWS Lake Formation** is a service that makes it easy to set up a secure data lake in days. It allows you to collect and classify data from disparate sources and set up a centralised catalog. This service prepares data for analyis via Amazon Redshift and other services.

### Analytics

- [**Amazon Redshift**](https://nightingalehq.ai/knowledgebase/what-is-aws-redshift) is a data warehouse service that integrates data from diverse services and allows you to query petabytes of data using SQL, to build business intelligence reports.
- **AWS Glue** is a fully managed extract, transform, and load (ETL) service that makes it easy to prepare and load data for analytics.
- **Amazon Athena** is an interactive query service that allows you to quickly analyse your data stored in Amazon S3 using standard SQL, paying only for the queries you run.
- **Amazon QuickSight** is a fast, cloud-powered business intelligence service that allows you to extract insights from your data and share them with relevant stakeholders via interactive report dashboards. With a pay-per-session pricing model, the service is a cost-effective way to introduce business intelligence to your organisation.

### AI and machine learning

- **Amazon Sagemaker** is a fully managed service that all of the components used for machine learning in a single toolset, enabling data scientists to build, train, and deploy machine learning (ML) models quickly and cost-effectively.
- **Amazon Personalize** is a machine learning service that can be used to introduce personalised content and product recommendations to applications.
- **Amazon Forecast** is a fully managed service that uses machine learning to deliver highly accurate forecasts. Even without any machine learning experience, you can use it to convert your time series data into projections of metrics such as product demand, resource needs, or financial performance.
- **Amazon Lex** is a service for building voice and text chatbots that can be integrated into applications. With an intuitive interface, you can create a chatbot in minutes, and take advantage of the advanced deep learning functionalities of automatic speech recognition and natural language understanding.  

## Security and Compliance

Cloud security is the "[highest priority](https://docs.aws.amazon.com/whitepapers/latest/aws-overview/security-and-compliance.html)" at AWS. Customers have access to a data center and network architecture built to meet the requirements of the most security-sensitive organizations, without the cost implications of managing such resources in-house.  

### Security

AWS services are built on secure global infrastructure and data is automatically encrypted when flowing through the global network. You can automate manual security tasks and have complete control over your data and who has access to it. Security products from AWS include **AWS Identity and Access Management (IAM)** and **AWS Security Hub**.  
Read more about AWS security [here](https://aws.amazon.com/security/).

### Privacy

AWS gives you ownership and control over your data, providing tools that allow you to determine where your data will be stored, secure it, and maintain control of who has access to your data and AWS services. There is also a security assurance program that uses best practices for global privacy and data protection to help you operate securely within AWS.  
Read more about AWS data privacy [here](https://aws.amazon.com/compliance/data-privacy-faq/).

### Compliance

AWS offers a collection of [Compliance Programs](https://aws.amazon.com/compliance/programs/) that guide users through comply with legal requirements, as well as services to support these resources, such as **Amazon Inspector** and **AWS Config**.  
Read more about AWS compliance [here](https://aws.amazon.com/compliance/).

## Pricing

AWS has a competitive, scalable pricing structure in which you only pay for the services you use. The monthly [pricing calculator](https://calculator.s3.amazonaws.com/index.html) helps you to budget and estimate costs of using AWS services.  

Creating an [account](https://aws.amazon.com/free/) on AWS provides:

- 12 months of free access to select services, including data storage and some AI services.
- Short term free trials of select services, including a two-month trial of Amazon Redshift.
- access to more than 20 always-free services, including 25GB of storage on Amazon DynamoDB.

Beyond the free services, AWS offers the choice between a pay-as-you-go (PAYG) model and reserved pricing. With either option, you are billed monthly for the services you use or reserve.  

### Pay as you go (PAYG)

A great option for scalability, with PAYG pricing you pay only for the AWS services you use. This allows you to innovate and adopt technologies rapidly without being hit by large set-up costs.  

The PAYG model differs between AWS services. For example, storage is charged by space used and number of requests, whereas machine learning services carry additional charges for training hours.  

Some services, such as S3, offer [tiered pricing](https://aws.amazon.com/s3/pricing/) that grants volume discounts when your usage increases beyond certain thresholds.

### Reserved pricing

If you are confident of the computing power and storage space you will need for your application in advance, the reserved pricing model can save you 30% of costs for select services. You can also choose to pay upfront for some or all of a year of reserved services for an even greater discount. The reserved pricing model is a good way to minimise risk and manage budgets, and there is even an [EC2 Reserved Instance Marketplace](https://aws.amazon.com/ec2/purchasing-options/reserved-instances/marketplace/) in case circumstances change.

### Purchase AWS services through an Amazon Partner Network (APN) partner

As well as having the option to buy directly from Amazon Web Services, you can go through an APN partner. APN partners can be Consulting Partners, who help customers to design and build AWS-based applications, or Techology Partners, who provide AWS-hosted software. Many APN partners are resellers of AWS services, which brings the benefit of having an expert on-hand to ensure you pay only for what you need, and local support. For those just starting out with cloud computing, a Managed Service Provider (MSP) can guide you through every stage of the cloud journey.  
You can find an APN partner [here](hhttps://aws.amazon.com/partners/find-a-partner/).  

### Support

All AWS accounts come with a free Basic support plan, which includes customer service, online communities and learning resources, access to the AWS Personal Health Dashboard, and access to the AWS Trusted Advisor service. Additional technical support becomes available when you purchase a Developer, Business, or Enterprise support plan, starting at $29 per month.  
Learn more about the support plans [here](https://aws.amazon.com/premiumsupport/plans/).
