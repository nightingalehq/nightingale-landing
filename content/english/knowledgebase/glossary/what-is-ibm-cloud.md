---
title: What is IBM Cloud?
date: 2020-04-09T12:50:18.038Z
author: Mia Hatton
description: >-
  A cloud computing solution such as IBM Cloud provides pay-as-you-go access to vast data storage and computing capabilities and allows you to scale the costs as you grow, avoiding expensive set-up costs.  
tags:
  - ibm cloud
  - cloud service
categories:
  - glossary
  - tech stacks
image: /images/uploads/ibm-cloud.png
executive: ''
departmenthead: ''
technical: ''
---
Welcome to the Nightingale HQ overview about IBM Cloud services. Here we aim to introduce people to what they need to know.

## Definition of IBM Cloud

From [IBM](https://azure.microsoft.com/en-gb/overview/what-is-azure):

> IBM Cloud™ is a robust suite of advanced data and AI tools, and deep industry expertise to help you on your journey to the cloud.

IBM Cloud is a [cloud service provider](https://nightingalehq.ai/tags/cloud-service). Other cloud vendors to consider are:

- [Microsoft Azure](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure)
- Amazon Web services ([AWS](https://nightingalehq.ai/knowledgebase/glossary/what-is-aws))
- [Google Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud)

## Key technologies

There are more than 100 products available from IBM Cloud, across 16 categories. These products include:

### Data storage and management

- There are several robust and durable object, block and file storage services including **Object Storage**, **File Storage**, and **IBM Cloud Backup**
- **IBM Lift** is a service that allows you to migrate your data via a CLI (command line interface)
- **IBM Db2 Warehouse on Cloud** is a full-managed, cloud [data warehouse](https://knowledge.nightingalehq.ai/data-warehousing) service
- There is a vast selection of SQL and NoSQL databases available, including **IBM Cloudant**, **IBM compose**, and **IBM Cloud Databases for PostgreSQL**  

### AI and machine learning

- **IBM Watson Studio** is a service that allows you to build and train AI models, and prepare and analyze data in a single, integrated environment
- **IB Watson Assistant** is a service for building and deploying virtual assistants
- **IBM Watson Machine Learning** is a service for creating, training and deploying machine learning models using an automated, collaborative workflow
- IBM Watson features a number of natural language processing services, including **IBM Watson Text to Speech**, **IBM Watson Speech to Text**, **IBM Watson Natural Language Understanding**, and **IBM Watson Tone Analyzer**
- **IBM Data Refinery** is a self-service data preparation tool for data scientists, engineers and business analysts

### Analytics

- **IBM Analytics Engine** is a combined Apache Spark and Apache Hadoop service for creating analytics applications
- **IBM Streaming Analytics** is a service for analysing a broad range of streaming text, video, audio, geospatial and sensor data
- **IBM Decision Optimisation** is a self-service decision environment designed to harness optimization-based support

## Security and Compliance

IBM Cloud features a suite of tools and services that ensure your data is secure on its platform. Visit the IBM Cloud Security centre [here](https://www.ibm.com/uk-en/cloud/security).

### Security

All of IBM Cloud's services are protected by its scalable suite of technologies and solutions for data security, including encryption at rest and in transit. Security products from IBM Cloud include **Network Security** and **IBM Cloud Data Shield**.  
Read more about IBM Cloud security [here](https://www-03.ibm.com/software/sla/sladb.nsf/sla/dsp).

### Privacy

IBM services are designed with data privacy at their core. Data access is strictly controlled and monitored across IBM Cloud services, and you have almost 60 data centres to choose from when setting up your services, so you can stay in control of your data's location.
Read more about IBM Cloud data privacy [here](https://www.ibm.com/cloud/privacy).

### Compliance

IBM Cloud infrastructure is approved by more than 30 compliance programmes. IBM Cloud offers a suite of resources to help you ensure compliance to these programmes when you set up your services.  
Read more about IBM Cloud compliance [here](https://www.ibm.com/uk-en/cloud/compliance).

## Pricing

IBM Cloud has a competitive, scalable pricing structure with several options to support businesses as they grow. From the XXXX you can access at-a-glance cost overviews to manage spending, and the IBM Cloud [pricing calculator](https://cloud.ibm.com/estimator) helps you to budget and estimate costs of using IBM Cloud services.

Creating a [Lite Account](https://www.ibm.com/cloud/free/) on IBM Cloud provides completely free access to more than 40 services on IBM Cloud, including:

- 25GB Object Storage per month
- 100MB data storage on Db2, an SQL database
- Thousands of API calls to IBM Watson AI services

IBM Cloud does not require a credit card to create an account, so you can get started right away.

Beyond the free services, IBM Cloud offers the choice between a pay-as-you-go (PAYG) model, reserved pricing, and subscription. With each option, you are billed monthly for the services you use or reserve.  

### Pay as you go

When you upgrade from a Lite Account to a PAYG account you receive $200 credit - valid for 30 days - to spend on any of the full offering of more than 190 services.  IBM Cloud products are charged by time, events and storage space, so your monthly bill always reflects your useage. 

You can view the PAYG pricing model of any service via the [pricing list](https://www.ibm.com/cloud/pricing/list).

### Reserved pricing

If you are confident of the computing power and storage space you will need for your application in advance, and you want to guarantee availability of virtual servers, the reserved pricing model is a great option for reliability and cost-effectiveness. In this model, you reserve the time and storage space required for the next year or two years, and make consistent monthly payments at reduced cost.

Read more about reserved pricing [here](https://cloud.ibm.com/docs/vsi?topic=virtual-servers-about-reserved-virtual-servers#about-reserved-virtual-servers).

### Subscription pricing

Subscription pricing is available for long-term commitments to IBM Cloud services, [on request](https://www.ibm.com/cloud/pricing).  

### Support

All IBM Cloud accounts come with a Basic support plan, as well as documentation, resources, and tutorials to help you get started. Additional and prioritised technical support becomes available when you purchase an Advanced or Premium support plan, starting at £200 per month. Learn more about the support plans [here](https://www.ibm.com/cloud/support).
