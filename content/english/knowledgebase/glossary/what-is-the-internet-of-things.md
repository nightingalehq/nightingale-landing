---
title: What is the Internet of Things?
date: 2020-04-09T11:56:18.553Z
author: Mia Hatton
description: >-
  The Internet of Things (IoT) refers to the connectivity of computing devices that allows them to interact with each other.
tags:
  - iot
  - data analysis
  - data collection
  - product development
categories:
  - glossary
image: /images/uploads/iot.jpg
executive: ''
departmenthead: ''
technical: ''
---

Welcome to the Nightingale HQ overview about the Internet of Things (IoT).
Here we aim to introduce people to what they need to know.  

## Definition of the Internet of Things

From [Wikipedia](https://en.wikipedia.org/wiki/Internet_of_things):  

> The Internet of things is a system of interrelated computing devices, mechanical and digital machines that are provided with unique identifiers and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction.  

To go a little further than things that are connected to each other via the internet, IoT is more commonly being used to refer to things that use sensors to [collect data](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-collection) and talk to each other, i.e. analyse the data and generate a reaction.  

## Everyday IoT

The internet of things is everywhere, and the best way to start explaining it is to have a look around you. If you have a wireless printer, a thermostat, or a virtual voice assistant, you have IoT devices within your home, and you can probably control them all from your tablet, phone or assistant, as they are all connected to the internet.  

Smart homes are increasingly being built with this technology in mind, so your lights, heating, security systems, appliances and much more can all be controlled in one place. But if your home didn't come smart, you can buy these things independently and enjoy the ease of asking your voice assistant to water the garden or print a recipe using your wireless printer.  

You might not have fully integrated smart appliances, but over 1 in 4 people currently own a virtual assistant, and the number is rapidly increasing, making voice assistants the most commonplace IoT devices. Wearable tech to monitor personal health is also a huge trend that uses IoT that is using sensors to bring people closer to their health.  

## Commercial IoT

Stepping outside of our homes, we find another wave of IoT applications appearing in supermarkets to healthcare facilities and in transportation. A few of these applications are very similar to our home comfort IoT, for example, smart buildings such as offices, stores or hotels that have multiple elements that can be controlled in one place, but other applications go a bit further.  

The [retail industry](https://blog.nightingalehq.ai/outstanding-ai-retail-techniques?hsLang=en) uses several forms of IoT, from asset tracking to manage inventories more efficiently and to keep a better check on lost and missing items. This works to reduce theft but also eases customers who have made online orders by giving real-time delivery updates. Retailers are even using this technology to send exclusive offers to app users when they are near a store. Finally, some supermarkets are creating a smarter check out system like [Sainsbury's Smart Shop](https://help.sainsburys.co.uk/help/products/smartshop-faq) that allows you to scan and go, or [Amazon Go](https://www.pocket-lint.com/phones/news/amazon/139650-what-is-amazon-go-where-is-it-and-how-does-it-work) stores where you can just pack your item straight into your bag and walk out.  

The internet of medical things (IoMT) or [Smart Healthcare](https://blog.nightingalehq.ai/ai-in-healthcare?hsLang=en), covers even more, from remote health monitoring to specialised devices like a smart inhaler or a pacemaker. Other applications include robots that cruise hospital halls delivering supplies, smart beds that can adjust to correctly support a patient or predictive maintenance for vital equipment.  

The world of transport also has plenty of IoT applications, from all the sensors that power self-driving cars, to technology that can help drivers find a free parking space in the city.  

## Industrial IoT

The [industrial internet of things](https://blog.nightingalehq.ai/ai-in-manufacturing?hsLang=en) (IIoT) refers to manufacturing and smart factories where sensors can be installed to equipment to gather insights about processes within the factory and optimise them. This can be applied to inventory management, augmenting design processes, conducting predictive maintenance and much more.  

Industrial IoT can also refer to farming practices, such as environmental monitoring to replicate optimal conditions for crop growth, automated irrigation systems and even livestock management.  

## Infrastructure IoT

Finally, IoT can be applied at the infrastructure level to create smart cities powered by smart energy, connected with smart transport. Public transport can be optimised using [IoT and digital twin concepts](https://blog.nightingalehq.ai/iot-technology-public-transport?hsLang=en) for better management of risks and delays, while sensors at [stops](https://www.opengovasia.com/smart-bus-stop-being-trialled-in-singapore-to-improve-commuter-experience/) can identify passengers with needs and adjust for them e.g. announcing the arrival of transport earlier to allow elderly passengers enough boarding time, purifying polluted air or providing air conditioning based on the weather.  

We are already seeing many cities roll out [connected street lights](https://iot-analytics.com/top-10-cities-implementing-connected-streetlights/), which can save millions in electricity costs as they only come on when required, [wireless smart grid sensors](https://www.gridsentry.us/) to optimise energy flow and identify issues before they occur, and [smarter materials](https://www.woodoo.com/) for construction to reduce the carbon footprint of the building industry.  

In another example, we see [smart bins](https://bigbelly.com/) that collect solar energy and use it to compress waste allowing for 8 times more rubbish to be collected before they need to be emptied. Better still they use wifi to let the council know when they are ready to be emptied, saving on recourses.  
