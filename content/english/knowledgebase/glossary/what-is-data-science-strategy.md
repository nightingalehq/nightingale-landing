---
title: What is data science strategy?
date: 2020-04-09T09:01:18.553Z
author: Mia Hatton
description: >-
  Data science strategy is as much about planning and executing your businesses, data and AI initiatives as it is about building internal skills and culture.
tags:
  - data science
  - strategy
  - training
  - data culture
categories:
  - glossary
  - offerings
image: /images/uploads/data-strategy.jpg
executive: >-
  Data science strategy goes far beyond analysing your data to make improved business decisions. It's about creating a thriving environment for data where it can be easily shared and accessed, safely stored and standardised, and viewed as a central resource to all departments. While it can be beneficial for a company to outsource data science skills, we recommend building up data skills within your team by upskilling and recruiting where possible as part of your data science strategy.  

  Data science strategy helps businesses:  

  - solve business problems

  - enhance day-to-day operations

  - advance the overall functioning

  - improve decision making

  - create a data-focused company culture

  - upskill teams

departmenthead: >-
  Data science strategy helps teams to work towards a common goal while ensuring a standardised way of working with data. It may also involve developing new skills within your team to facilitate your data efforts. 

  You may need this service if:  


  - your team is struggling to make data-driven decisions efficiently, despite access to data.

  - your team lacks the skills to analyse data or set up data cycles.

  - your team has an ad-hoc or reactive approach to using data to meet your business goals.

  - your business suffers from data silos due to friction between departments or technological factors, which prevents teams from seeing the bigger picture.


  KPIs you should consider measuring for this are:  


  - Quicker turnarounds due to more efficient decision-making.

  - Increased efficiency when data and insights are shared between departments.

  - Increased revenue from acting on data insights.

  - Reduced costs due to more efficient data collection, storage and analysis.

technical: >-
  Data science strategy provides the whole organisation with a roadmap to being data-driven. Technical teams need to be involved in setting the data science strategy to ensure that it is realistic and that the vision leads to a centralised data platform in which data is stored securely and efficiently, and which provides actionable insights to everyone who needs them.

  Data science strategy helps deliver:  

  - enhanced data skills among all staff.

  - improved data modelling and database structure.

  - [business intelligence](https://nightingalehq.ai/knowledgebase/glossary/what-is-business-intelligence) and AI adoption.

  
  Get this service if you encounter:  

  - a lack of cohesion between departments when it comes to using data.

  - a lack of understanding of data insights.

  - ad hoc or reactive requests for data insights, which can lead to inefficiencies.

  
  Key criteria to consider are:  

  - What tools should the organisation implement to store data securely and efficiently?

  - How should business intelligence be incorporated into the strategy and which tools will be required?

  - Will your database structure need to be changed to meet the data science strategy?

  - What are the data needs of all departments and how can the data science strategy meet them?

  - How can the organisation adopt a data culture?

  - Who should receive training and how should the training be delivered?

---

Welcome to the Nightingale HQ overview of data science strategy services. Here we aim to introduce people to what they need to know.  

## Definition of data science strategy

Data science strategy refers to a company's vision for how data will be used to help achieve the company's business goals, how to build a thriving data culture, and how to address the skills and knowledge required to execute this vision.
