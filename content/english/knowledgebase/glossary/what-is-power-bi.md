---
title: What is Power BI?
date: 2020-04-09T11:56:18.553Z
author: Mia Hatton
description: >-
  Power BI is a business intelligence/data visualisation service by Microsoft. It integrates data from several sources allowing users to create reports and dashboards featuring interactive charts and share them with different teams.  
tags:
  - visualisation
  - insight
  - data analysis
  - business intelligence
categories:
  - glossary
  - tech stacks
image: /images/uploads/power-bi.png
executive: ''
departmenthead: ''
technical: ''
---

Welcome to the Nightingale HQ overview about Power BI services.
Here we aim to introduce people to what they need to know.  

## Definition of Power BI

From [Microsoft](https://powerbi.microsoft.com/en-us/what-is-power-bi/):  

>Power BI is a business analytics solution that lets you visualize your data and share insights across your organization, or embed them in your app or website. Connect to hundreds of data sources and bring your data to life with live dashboards and reports.  

## Does your organisation need Power BI?

Power BI helps teams implement [business intelligence](https://nightingalehq.ai/knowledgebase/glossary/what-is-business-intelligence) with ease. It is relatively simple to start building dashboards of interactive visualisations from a range of data sources, so you can start to see actionable insights very quickly. Being able to share reports and dashboards with select teams gives Power BI the advantage of making relevant insights available to the right people quickly.  

You may need Power BI if:

- you want to make use of business intelligence for data-driven decision making
- you want to be able to share insights with different groups and teams
- you have difficulty gaining insight from data that is spread across several storage locations or comes from disparate sources
- your IT team is burdened by frequent requests for data access from different departments

## Benefits of Power BI

The benefits of Power BI to your business include:

- Data insight dashboards can be shared with select teams and groups, making actionable insights into all areas of business performance available to relevent personnel
- It allows you to seamlessly connect disparate data sources to produce a holistic view of your business
- It delivers powerful [data modeling](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-modeling) and [visualisation](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-visualisation)
- It empowers your teams to act on personalised insight dashboards
- The powerful search function allows you to ask questions of your data in natural language to get intelligent insights

Power BI can also be used to publish interactive data visualisations, for example by integrating them into your application for your users to explore.  

While use of Power BI desktop is free, adopting shared dashboards on the Power BI service comes at a cost. You can measure your return on investment by considering the following KPIs:

- Increased sales after implementing business intelligence
- Reduced costs when insights gained from Power BI are implemented to improve efficiency
- Reduced time spent analysing data

## Technological Considerations  

### Getting Started with Power BI  

Creating a [Power BI Service](https://powerbi.microsoft.com/get-started) account and installing [Power BI Desktop](https://powerbi.microsoft.com/en-us/downloads/) are both completely free.  

Microsoft provides comprehensive documentation to help you get started with Power BI, but you may find that to get the most out of it you will need to provide training for the staff who use it. You should take the cost of training into consideration when deciding if Power BI is right for your business.  

People who are proficient with Excel - especially Power Pivot and Power Query - will find it easier to start using Power BI than those who are not.  

## Integrations

One of Power BI's main strengths is the huge number of data sources to which it can connect. With more sources added monthly - and the option to develop your own Power BI connectors - whatever your data source, you can almost certainly bring your data into Power BI.  

Currently supported data sources include:

- Microsft products including Excel, Access and Sharepoint
- [Azure](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure) services including Azure SQL Database, Azure SQL Data Warehouse, Azure Blob Storage, and Azure Table Storage
- Google Analytics
- Facebook
- Oracle
- MailChimp
- Salesforce
- SQL Server
- .csv and .txt files

View the full list [here](https://docs.microsoft.com/en-us/power-bi/power-bi-data-sources).  

### Pricing

Power BI desktop software can be installed for free, providing you with instant access to powerful data modeling and visualisation tools and connect data from many different sources. Providing tiered access to dashboards to teams requires a subscription to the Power BI service from £7.50 per user monthly.  

### Alternatives to Power BI

Other business intelligence tools do exist that perform similar functions to Power BI.  

- [Sisense](https://www.sisense.com/en-gb/) boasts unmatched time-to-insight and makes it easier to embed your visualisations than Power BI does
- [ThoughtSpot](https://www.thoughtspot.com/) offers an AI-driven, search-based alternative to Power BI
- [Tableau](https://www.tableau.com/) comes at a higher cost than Power BI but integrates with more data sources and can handle greater volumes of data, so might be a better option if you anticipate high volumes and don’t have the time or resources to develop custom connectors for your data
