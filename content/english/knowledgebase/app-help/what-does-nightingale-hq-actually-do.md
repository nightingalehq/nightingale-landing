---
title: What does Nightingale HQ actually do?
date: 2020-04-14T13:13:18.553Z
author: Mia Hatton
description: >-
  Nightingale HQ is a diverse group of individuals who share the same goal - to make Your Business and Your People AI-ready.
tags:
  - nightingale hq app
categories:
  - app help
image: /images/uploads/nhq-logo.png
executive: ''
departmenthead: ''
technical: ''
---
Nightingale HQ is a diverse group of individuals who share the same goal - to make Your Business and Your People AI-ready. This emerging tech will be integrated into every business in the next few years and we are developing a platform that combines a robust set of data, AI and business expertise. Our track record lies in helping individuals and organisations generate value, both cost saving and revenue generating no matter what stage of the maturity curve they are at. Our platform consists of three core products:

- [AI Learn](https://nightingalehq.ai/products/ai-learn/) offers online and on-demand courses and mentoring programmes to help build core competencies
- [AI Connect](https://nightingalehq.ai/products/ai-connect/) uses our AI algorithm to match you to the right expertise and skills to help deliver AI projects
- [AI Direct](https://nightingalehq.ai/products/ai-direct/) (in development) is our leadership toolkit that supports managers on the road to change with AI. We offer a set of practical tools and workshops for individual managers and teams to accelerate their strategic efforts.

Read more on our [About Page](https://nightingalehq.ai/about/)

## We are the catalyst company in helping businesses adopt AI.

We understand the pain points and challenges that modern businesses face around digital transformation, particularly artificial intelligence, and we want to make it easy for organisations to make the connections they need in order to excel with their data projects.

## AI Connect Lite

Our marketplace allows organisations and business leaders to find the perfect partner matches and go on to connect with these consultants outside of the app. This takes out the stress and guesswork of finding the right minds for your project and ensures you work with a consultant that can offer the best guidance on what you want to achieve.  

On the other side, partners who use Nightingale HQ can increase their visibility and be more easily found by the leads that matter. Our matching algorithm takes into account all the details from tech stacks and verticals to offerings and location, meaning that by the time a lead makes contact, you already know that you're a good match for each other and can get down to the details without wasting each other's time.  

## Future features

As we roll out more features, we plan to introduce more ways to make it easier for businesses to adopt AI, such as an AI readiness assessment tool and plenty more resources to get businesses going.
