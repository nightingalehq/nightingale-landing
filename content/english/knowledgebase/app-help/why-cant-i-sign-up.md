---
title: Why can't I sign up?
date: 2020-04-14T13:01:18.553Z
author: Mia Hatton
description: >-
  You must be using a company email account.
tags:
  - nightingale hq app
  - help for partners
  - help for organisations
categories:
  - app help
image: /images/uploads/sign-up-free.png
executive: ''
departmenthead: ''
technical: ''
---
In order to access Nightingale HQ, you must be using a company email account as opposed to a personal one. You may receive an error message if you try to sign up with a personal email address.
