---
title: What is a Vertical?
date: 2020-04-14T08:33:18.553Z
author: Mia Hatton
description: >-
  A vertical is an industry or market.
tags:
  - nightingale hq app
  - help for partners
  - partner
  - tags
categories:
  - app help
image: /images/uploads/vertical.jpg
executive: ''
departmenthead: ''
technical: ''
---
Business verticals are narrow markets whose specific needs make them especially likely to want your offerings.  

An industry vertical (also called a vertical market) is more specific, identifying companies that offer niche products or fit into multiple industries.  

If there is a vertical you specialise in but isn't available to select then use the 'tags' section of your profile to add it. We regularly review the custom tags and promote them to the relevant section. The tags are also included in search immediately so you won't miss out on any traffic.  
