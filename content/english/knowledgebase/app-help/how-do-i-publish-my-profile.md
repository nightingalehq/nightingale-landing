---
title: How do I publish my profile?
date: 2020-04-14T08:37:18.553Z
author: Mia Hatton
description: >-
  Login, navigate to your profile, update it, and hit the Publish button.
tags:
  - nightingale hq app
  - partner
  - help for partners
categories:
  - app help
image: /images/uploads/partner.png
executive: ''
departmenthead: ''
technical: ''
---
Login, navigate to your profile, update it, and hit the Publish button.
