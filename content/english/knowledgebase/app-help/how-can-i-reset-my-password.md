---
title: How can I reset my password?
date: 2020-04-14T09:15:18.553Z
author: Mia Hatton
description: By visiting the 'Forgot Password' page on the app.
tags:
  - nightingale hq app
  - help for organisations
  - help for partners
categories:
  - app help
image: /images/uploads/forgot-password.png
executive: ''
departmenthead: ''
technical: ''
---
Visit our [Forgot Password](https://id.nightingalehq.ai/Account/ForgotPassword) page to get a password reset link sent to your email address.
