---
title: How do I know I can trust Nightingale HQ?
date: 2020-04-14T12:334:18.553Z
author: Mia Hatton
description: We respect the terms of our privacy policy and operate under GDPR compliance.
tags:
  - nightingale hq app
  - help for organisations
  - help for partners
categories:
  - app help
image: /images/uploads/trust.jpg
executive: ''
departmenthead: ''
technical: ''
---
Nightingale HQ takes your data and your privacy very seriously and always operate within compliance of the GDPR. For more details on how we collect and store data, please refer to our full [Privacy Policy](https://https://nightingalehq.ai/privacy/).  

The information that you share on your profile may be visible to other members at certain times, however, your personal details will always be kept private. For example, as an organisation contacting a consultant, your profile details will become available for the purpose of engagement. As a consultant, your profile information is visible only to customers who have signed up to use the app.  

For more details on the privacy of your information, check out the following article: [Is my information public](https://nightingalehq.ai/knowledgebase/glossary/is-my-information-public)?
