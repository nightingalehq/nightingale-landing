---
title: Can I contact partners?
date: 2020-04-14T09:15:18.553Z
author: Mia Hatton
description: Definitely! Message them, go to their site, or tweet them.
tags:
  - nightingale hq app
  - partner
  - help for organisations
categories:
  - app help
image: /images/uploads/contact.jpg
executive: ''
departmenthead: ''
technical: ''
---
After we've shown you the partners who match your needs, you probably want to get in touch with them after taking a look at the info they have provided.

Each partner provides us with contact mechanisms for them. The Message option is the most straight forward. This sends them an email which will include your info so they can get up to speed with helping you quickly.

![screenshot of Nightingale HQ AI Connect app with arrow showing 'contact partner' button](/images/uploads/contact-partner.png)
