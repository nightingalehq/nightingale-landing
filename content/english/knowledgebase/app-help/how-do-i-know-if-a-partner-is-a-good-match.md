---
title: How do I know if a partner is a good match?
date: 2020-04-14T07:28:18.553Z
author: Mia Hatton
description: >-
  Your top matches will appear at the top of the 'Partner' page.
tags:
  - nightingale hq app
  - partner
  - help for organisations
categories:
  - app help
image: /images/uploads/is-partner-good-match.jpg
executive: ''
departmenthead: ''
technical: ''
---
Our matching algorithm will show you your best matches at the top of the 'Partner' page based on the parameters in your profile. You also have the option to search for partners using a keyword or add additional search criteria on the left-hand side of this page, which may further filter down the matches that you see.

![screenshot of Nightingale HQ AI Connect app showing 'Partner' page](/images/uploads/partner-match.jpg)

If you are not seeing any available matches, you can still scroll through available consultants, view their profiles and click through to their websites to find out more.

You will be able to see the rating of a partner if they have worked with and been reviewed by someone through Nightingale HQ.
