---
title: What is a Partner?
date: 2020-04-14T08:56:18.553Z
author: Mia Hatton
description: >-
  We use the word partner a lot so here's a definition!
tags:
  - nightingale hq app
  - partner
  - help for organisations
categories:
  - app help
image: /images/uploads/partner.png
executive: ''
departmenthead: ''
technical: ''
---
A partner is a business that provides services around data and AI. They are typically consultancies of 1-200 people and have a specialism in a particular area.  

Find a partner with particular expertise immediately when you sign up at [Nightingale HQ](https://app.nightingalehq.ai/). Just fill in your profile and navigate to the PARTNERS page to browse your top matches (based on your profile details), or add your own search criteria in the search box on the left-hand side to update the results.  

![Screenshot of Nightingale HQ's AI Connect app with partner tiles](/images/uploads/partner-match.jpg)

Read more about becoming a partner [here](https://nightingalehq.ai/partners).
