---
title: What is an Introducer Agreement?
date: 2020-04-14T07:28:18.553Z
author: Mia Hatton
description: >-
  An introducer agreement is designed for use in situations where a supplier of goods or services wishes to engage another as an introducer of clients or new suppliers.
tags:
  - nightingale hq app
  - help for partners
categories:
  - app help
image: /images/uploads/agreement.jpg
executive: ''
departmenthead: ''
technical: ''
---
The introducer agreement is an agreement between you as a partner and us as a provider of the AI Connect service. We ask all partners to read and accept the introducer agreement before using the service.
