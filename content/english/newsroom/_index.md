---
title: "Newsroom"
date: 2020-03-02T12:52:36+06:00
image_webp: images/blog/blog-post-3.webp
image: images/blog/blog-post-3.jpg
author: Ruth Kearney
description : "Accelerate your AI projects"
menu:
  resources:
    weight: 6
  main:
    parent: company
    weight: 6
---

Welcome to our company updates section. Here you will find major announcements and associated press releases.

> If you'd like to include us in an article or publication, use the [Press kit](../press) for convenience.
