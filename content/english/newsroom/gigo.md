---
title: 'Garbage In, Garbage Out – The pitfalls of bad data'
date: 2019-08-26T00:00:00.000Z
description: 'Garbage In, Garbage Out GIGO'
tags:
  - Awards
categories:
  - Garbage In
  - Garbage Out GIGO
image: images/NGHQ_ICON_COLOUR.svg
author: Ruth Kearney
---
**Garbage In, Garbage Out**

What is it? In advance of our upcoming Data Science Bootcamp, we are pleased to announce an open evening to explore the importance of data quality. Talent Garden faculty member, Steph Locke – data scientist and Microsoft AI MVP talks Garbage In, Garbage Out – The pitfalls of bad data.

Steph takes on the importance of having good data especially for AI subsets like machine learning and deep learning, which gain greater capabilities over time by analyzing large sets of data, learning from them and making adjustments that make the applications more intelligent.

But what if the data is biased, corrupted or wrong? This workshop explores how to clean your data to safeguard data quality and ensure your insights are gold, not garbage.

**Who is it for?** This workshop is ideal for anyone interested in learning more about the Data Science Bootcamp and how this course can accelerate their career. You might be an IT professional, a Finance Specialist, Business Analyst, Data Analyst, Researcher, Academics, Software Engineer or simply interested in learning more about the data science discipline.

**Key Takeaways:** Horror stories to help convince others about data quality
Insight into incentive structures for front-line staff re: quality data input
An understanding of user experience and usability methods for improving data entry
An awareness of post-collection cleaning and management techniques to improve quality

**Faculty Bio** Steph Locke is one of only three individuals in the world to be recognised with both Microsoft’s Artificial Intelligence Most Valued Professional (MVP) award and their Data Platform MVP award. She is the founder of Locke Data, a UK-based data science consultancy, and Nightingale HQ, an online platform connecting data science and AI consultancies to businesses who need their expertise.

## Article first published online [here](<## https://talentgarden.org/agenda/garbage-in-garbage-out-the-pitfalls-of-bad-data/>)

[Press kit](../../press)