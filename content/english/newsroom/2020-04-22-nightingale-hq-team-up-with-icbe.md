---
title: "Nightingale HQ team up with ICBE "
date: 2020-04-22T11:50:49.223Z
author: Ruth Kearney
description: New collaboration with Irish Centre For Business Excellence ICBE
tags:
  - AI
categories:
  - News
image: /images/uploads/icbe-rgb-01-1-.jpg
---
[Nightingale HQ](https://nightingalehq.ai/) team up with **Irish Centre for Business Excellence Network (ICBE) Advanced Productivity Skillnet** to bring three webinars on AI in Manufacturing as part of the [\#AIFightsBack](https://blog.nightingalehq.ai/announcement-aifightsback-webinar-series) series. 

\#AIFightsBack in Manufacturing focus on providing practical insights and real use cases in **manufacturing**. With AI set to become a key differentiator in manufacturing processes, it is critical that you upskill and reskill to stay ahead of the competition. Read about the full series [here](https://blog.nightingalehq.ai/announcement-aifightsback-webinar-series "https\://blog.nightingalehq.ai/announcement-aifightsback-webinar-series") and the manufacturing webinars are as follows;

**1. AI for Manufacturing - Thursday 23 April at 15.00 - 16.00**

AI is already having a significant impact on manufacturing and those who are getting it right will reap real benefits. It’s estimated that there is a 4-10% EBITDA increase from predictive maintenance AI solutions alone. AI is set to become a key differentiator in manufacturing processes and you need to stay ahead of the competition.

Data Scientist Steph Locke focuses on a set of practical ways AI save time and money in manufacturing, from optimising processes, predicting maintenance or enforcing quality control.

**[Register here](<https://www.eventbrite.co.uk/e/ai-in-manufacturing-aifightsback-online-seminars-tickets-100820151788>2.)**

**2. The Historian and AI - Thursday 14 May 2020 15.00 - 16.00**

AI for manufacturing has huge potential. As well as clear AI use cases like robotics and automation, the wealth of data being consolidated into industrial time series via historian appliances presents an opportunity for further AI applications.

Steph talks about consolidating data and practical applications for manufacturing such as how to build early warning systems for critical issues, optimise maintenance programs, and improve processes. Learn from relevant use cases start to identify how much AI can add value to your business.

**[Register here](<https://www.eventbrite.co.uk/e/the-historian-and-ai-aifightsback-online-seminars-tickets-100915553136>)**

**3. AI for Manufacturing – A Game Changer for Techies, 21 May 15.00- 16.00**

This session is for technical employees in manufacturing who need to learn more about building ai systems for manufacturing processes. Steph will cover key topics including;

* How to optimise flow through the manufacturing process
* Identifying quality issues
* Minimising downtime for machinery
* Consolidate existing data and what off-the-shelf AI solutions
* Moving beyond POC and scaling AI.

**[Register here](<https://www.eventbrite.co.uk/e/ai-for-manufacturing-technical-aifightsback-online-seminars-tickets-100913976420>)**

**About**

**AIFightsBack** is a free webinar series runs over 12-weeks and is for anyone in business looking to learn how AI can create value, and help them come back stronger than ever. Read about the full series [here](https://blog.nightingalehq.ai/announcement-aifightsback-webinar-series "https\://blog.nightingalehq.ai/announcement-aifightsback-webinar-series")

**ICBE** is a not-for-profit organisation established by leading companies in Ireland with a deep commitment to continuous improvement. Their vision is to be the business-excellence and knowledge-sharing network of choice for the manufacturing and services sectors in Ireland and to maximise their members’ success and Ireland’s recovery, growth and competitiveness. 

![ICBE and Nightingale HQ](/images/uploads/logo.png "ICBE collaboration ")