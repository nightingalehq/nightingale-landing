---
title: FBS Small Business Awards 2020
date: 2020-01-23
image: 
tags:
  - Awards
categories:
  - Awards
author: Ruth Kearney
---

**FBS Small Business Awards 2020**

**Tell us briefly about you and your business**

Nightingale HQ is a platform for businesses to adopt AI. As the supply of data in all industries increases exponentially, we help businesses get AI-ready so that they can fully harness and utilise the data available to them to solve business problems. Nightingale HQ can help get your business the training and connections they need to start practising data science.

Provide an overview of how the business launched and why it is filling a gap in the market
Nightingale HQ emerged when Steph Locke, CEO, realised that in order to help more people in her original business, Locke Data, she would need to scale, and rather than scale her consultancy, she would achieve this instead by creating a platform.

The first step was to create the consultancy marketplace where both organisations and consultancies can register. Applying a matching algorithm, organisations that are looking for help with a project will be shown their top matches, based on the details they have entered such as the tech stacks they work with, their industry, etc. Partners with best matches to these requirements appear first on the list.

With this functionality set up, we are now looking at developing the training section of the platform.

Unlike the competitors who focus on the freelance market, Nightingale HQ provides valuable connections with consultants who are better equipped to guide AI development and build internal skills, which are more valuable to these organisations in the long term.

**Evidence your business success since launch**
The initial talks for making Nightingale HQ real began in December 2018, so 2019 has been a heavy year for getting things off the ground. Our team remained a team of 2 right up until September, so Steph and co-founder Sarah worked very hard to lay the groundwork and create the website and web-app. By June the platform went live and by the end of the year, we had 64 consultancies of varying sizes signed up to the marketplace.

We were able to grow our team in September, taking on two software development interns in September, one of which we kept on, a data science apprentice and a graduate digital marketer, taking the pressure off the founders and expanding the capabilities of our team. With the turn of the new year, we also added a senior salesperson to assist with the next phase of development.

**Explain why this is impressive compared to other start-ups**
At such early stages, it is difficult to compare our progress to other startups. We are pleased to have made it past the one year mark, especially considering that a quarter of start-ups don't make it past this point. On average, it takes 4 years to start making money with a startup, and up to 10 years to really establish yourself as a business. Our next goal is to be revenue generate, which we aim to achieve with our next stage of development.

It has been a harsh investment market for businesses due to external factors like Brexit, so despite successful pitches and interest from investors, and lots of support including accelerator schemes and access to mentors, we haven’t received any additional funding and have got where we are today entirely off the back of our founders, which is quite impressive.

Share your evidence of successful start-up performance and demonstrate that you have built a solid foundation upon which your business can thrive
Despite the harsh environment for startups, we have a lot of stats in our favour. Odd for success improve when a company has two founder rather than one, when the found has a bachelor’s degree of higher (true of both of our founder) and chances of scaling too soon are reduced. Female founders are on the rise, and the niches of software and tech are among the strongest for startups in this time.

Stats aside, we have built a strong foundation for our platform and learned from what has and hasn’t worked well. While one side of model seemed to work really well with attracting talented consultants to work with, the other side has gone a little slower, so we have adjusted for this in our next stage of development, creating a product targeted at a slightly different audience. Maintaining the momentum of what we have built so far while adding an extra layer of functionality to create a more complete AI-readiness platform, we believe we have the elements in place to thrive in the coming years, as long as we continue to put in the hard work.

Article first published https://www.fsbawards.co.uk/
