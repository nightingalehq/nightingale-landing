---
title: Company
menu: 
  main:
    weight: 2
    identifier: company
---

Nightingale HQ is a platform for businesses to adopt AI. As the supply of data in all industries increases exponentially, we help businesses get AI-ready so that they can fully harness and utilise the data available to them to solve business problems. Nightingale HQ can help get your business the training and connections they need to start practising data science and machine learning.

- [About](../about)
- [Team](../team)
- [Products](../products)
- [Newsroom](../newsroom)
- [Press kit](../press)
- [Blog](//blog.nightingalehq.ai)
- [Knowledgebase](//knowledge.nightingalehq.ai)