---
title: "Press"
date: 2019-12-09T17:01:46Z
draft: false
menu:
  resources:
    weight: 3
  main:
    weight: 4
    parent: company
---

{{< toc >}}

## Fact Sheet
- Nightingale HQ Ltd
- Based in Cardiff, Wales, UK
- Address: 29 Gelliwastad Road, Pontypridd, Wales, UK CF37 2BN
- Website: [nightingalehq.ai](//nightingalehq.ai)
- Press / Business contact: [press@nightingalehq.ai](mailto://press@nightingalehq.ai) & [founders@nightingalehq.ai](mailto://founders@nightingalehq.ai)
- Social
    + Twitter: [@nightingalehqai](//twitter.com/nightingalehqai)
    + Facebook: [nightingalehq](//facebook.com/nightingalehq/)
    + LinkedIn: [nightingale-hq](//linkedin.com/company/nightingale-hq/)


## Description
Nightingale HQ is a startup developing an online platform to help businesses face the challenge of adopting Artificial Intelligence (AI).

## History
Nightingale HQ is an online platform designed to help businesses become early adopters of artificial intelligence (AI). From chatbots, data management, customer insight and deep learning, AI will become more prevalent as big data and IoT technology further integrate itself into day-to-day life.

Nightingale HQ was founded at the very end of 2018 and after several months of beta testing the business launched in June (2019) and is now continuing its product development with the aim to offer services beyond its competitors, including technology giants Microsoft.

Despite the business being pre-revenue, it is growing and is currently recruiting for three new positions, one of which is a Data Scientist apprenticeship in collaboration with Cardiff MET University. Nightingale HQ is also a finalist at the Pitch It Valleys event where they will compete for a potential £75k investment.

## Videos
{{< youtube fflyrntX7dM >}}

## Images
- [Nightingale HQ Images - HD.zip](../Nightingale%20HQ%20Images%20-%20HD.zip)

## Logo
&copy; All copy rights and trademarks asserted - Nightingale HQ Limited 

Get all logo variants at [the Nightingale HQ Assets Github Repository](//github.com/nightingalehq/nightingalehq-assets). We also provide a [PDF of our colour palette](https://github.com/nightingalehq/nightingalehq-assets/raw/master/NightingaleHQ-Palette-v1.pdf) which gives the different values required for our colour palette.


- Our first preference is to use our full colour wordmark where possible.
- Where possible, use the full colour versions
- When full colour isn't suitable, we have white versions
- We provide all of our visual assets in PNG, JPG PDF, and SVG format 
- You should only use the PNG's in small format. 
- For online use, we provide RGB versions of our assets
- For print use, we have PRINT versions of our assets
- Please do not modify our assets in any way

{{< figure src="//github.com/nightingalehq/nightingalehq-assets/raw/master/rgb/NGHQ_WORDMARK_COLOUR.png" alt="Nightingale HQ wordmark" width="50%">}}

{{< figure src="//github.com/nightingalehq/nightingalehq-assets/raw/master/rgb/NGHQ_ICON_COLOUR.png" alt="Nightingale HQ full colour logo" width="25%">}}



## Awards & Recognition
{{< figure src="../img/Wales Technology Awards Finalist.jpg" link="http://estnet.uk.net/news/finalists-announced-for-record-breaking-wales-technology-awards-2019/" alt="Finalist in the ESTNet Sir Michael Moritz Tech Start-up award" title="Finalist in the ESTNet Sir Michael Moritz Tech Start-up award" >}}

<!-- ## Selected articles -->

## Team

- Steph Locke
    + CEO
    + Twitter: [@theStephLocke](//twitter.com/theStephLocke)
    + LinkedIn: [stephanielocke](//uk.linkedin.com/in/stephanielocke)
    + GitHub: [stephlocke](//github.com/stephlocke)
    + Amazon Author: [stephanielocke](//amazon.com/author/stephanielocke)
    + Microsoft: [Steph Locke AI MVP award](//mvp.microsoft.com/en-us/mvp/Stephanie%20%20Locke-5001721)
- Ruth Kearney
    + Product & Commercial Dirctor
    + Twitter: [@ruthtagged](//twitter.com/ruthtagged)
    + LinkedIn: [ruthkearney](//www.linkedin.com/in/ruthkearney/)
- Sarah Williams
    + CTO
    + Twitter: [@ff0brickcode](//twitter.com/ff0brickcode)
    + LinkedIn: [sarah-williams-cardiff](//www.linkedin.com/in/sarah-williams-cardiff/)
    + GitHub: [yellowbrickcode](//github.com/yellowbrickcode)

## Contact
- Inquiries: [press@nightingalehq.ai](mailto://press@nightingalehq.ai)
- Twitter: [@nightingalehqai](//twitter.com/nightingalehqai)
- Facebook: [nightingalehq](//facebook.com/nightingalehq/)
- Web: [nightingaleHQ.ai](//nightingalehq.ai)