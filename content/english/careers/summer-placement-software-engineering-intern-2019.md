---
title: "[CLOSED] Software Engineering Intern"
seo:
  title: "Software Engineering Intern"
  description: ""
date: 2019-07-01T17:01:46Z
expirydate: 2019-08-01T17:01:46Z
draft: false
layout: "wrapped"
weight: 1
extract: Get hands-on with a modern tech platform in this 4-12 week software engineering internship.
---

The intern will work at Nightingale HQ for four to twelve weeks, depending on availability, on developing the Nightingale HQ platform. Helping with the JavaScript web application and developing code to integrate our application with our CRM solution and existing .NET API, the intern will help us accelerate our development of this startup, whilst getting real experience of in-demand technologies.

The placement can start as early as 15th July and can run until October or when the semester starts.

We are committed to paying all interns at the National Living Wage. During the intern’s time with Nightingale HQ, we will also cover the costs of useful training.

People from any age, employment history, and education background with some existing knowledge of JavaScript, and ideally .Net, are welcome to apply. 



## Takeaways from the role
- Experience developing a real, in-production solution
- A network of tech startups and other coworking businesses
- Building a startup, via real experience and training

## Duties & responsibilities
- Develop additional functionality in our Vue web app
- Enhance the User Experience of our Vuetify based UI
- Develop Azure Functions in Node or .Net to manage communication between our CRM solution
- Develop additional API endpoints of our .Net Core back-end

## Criteria
### Essential criteria
- Comfortable with software development principles
- Some previous experience writing code

### Desired criteria
You’ll stand out if you have any or all of these:

- Prior work experience in a software engineering role
- A track record of self-paced learning in coding
- Able to write any TypeScript, Vue.js, Node.js, C#, .Net, and .Net Core
- Knowledge of git, unit testing, continuous integration, and continuous deployment
- An interest in Artificial Intelligence

## The details
- 35 hours a week
- £16,380 per annum (£315 per week)
- Based in central Cardiff or Caerphilly. Some remote working is available
- Minimum 4 weeks, maximum 12 weeks
- Flexible working around core hours of 10 – 3
- Reporting to Sarah Williams CTO

## The application process
Please send us an email with your CV as an attachment to [people@nightingalehq.ai](mailto://people@nightingalehq.ai).

### CV & submission tips
- There's no need to write a large cover letter for us but if you'd like to put extra info in the email, we recommend taking the headers or criteria from the ad and writing points specific to them
- Please don't include pictures, your address, date of birth, your NI number, your references personal information and so on. The less personally identifiable information we have to store, the better!
- CVs are great when they're one or two pages. We won't discount longer CVs and we will offer CV feedback to any applicant who wishes it. For what Steph, the CEO, thinks a good CV looks like, you can check out [her online CV template](https://www.overleaf.com/articles/steph-lockes-one-page-cv/wbttfmfqcdtq).

### The process
We will be shortlisting applicants and do an online call as a first interview. This first interview will go through the role's criteria and provide a baseline assessment. The next stage will be a face to face interview with the founders to understand what unique perspective you would bring to the role.