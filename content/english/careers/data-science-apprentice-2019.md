---
title: "[CLOSED] Data Science Apprentice"
seo:
  title: "Data Science Apprentice"
  description: ""
date: 2019-07-01T17:01:46Z
expirydate: 2019-08-01T9:01:46Z
draft: false
layout: "wrapped"
weight: 1
extract: Get hands on experience as a data scientist and gain a degree whilst doing so.
---

The Data Science Apprentice will work at Nightingale HQ for three years, during which time they will attain the Applied Data Science BSc from Cardiff Metropolitan University. It would be ideal for someone returning to work, someone coming out of their A Levels and thinking about next steps, or a recent graduate looking to cross-train into data science.

The apprenticeship will start early September 2019.
Under the Degree Apprenticeship scheme, the apprentice will typically do a day of study at the university and work four days a week at Nightingale HQ performing the role of a junior data scientist.

We are committed to paying all apprentices at the National Living Wage. The degree will be funded by Welsh Government, so no loans or additional expense will be required to get the degree.
People from any age, employment history, and education background with an interest in becoming a data scientist are welcome to apply. 



## Takeaways from the role
- 3 years of experience as a data scientist
- A network of tech startups and other coworking businesses
- Building a startup, via real experience and training
- An open portfolio of results
- An Applied Data Science BSc

## Duties & responsibilities
As the apprentice gains more knowledge, more of the tasks will become part of their day to day responsibility.

- Develop and manage business reporting
- Contribute to company publications on data science and artificial intelligence
- Perform statistical data analysis and contribute to data driven decision making
- Develop and integrate machine learning processes into all areas of the business

## Criteria
### Scheme criteria
- Must reside in Wales
- Must not hold an IT-related degree already

### Essential criteria
- Comfort with mathematics e.g. a Mathematics A Level, portfolio pieces, online courses etc
- Some previous experience of employment
- Strong written English skills

### Desired criteria
You’ll stand out if you have any or all of these:

- Prior work experience in data, business intelligence, statistics, machine learning, or artificial intelligence
- A track record of self-paced learning in the data science space
- Experience in writing articles, presenting, or blogging
- A higher education qualification in a numerate field e.g. economics, sciences, social sciences

## The details
- 35 hours a week
- 1 day a week to be spent at the university
- £16,380 per annum
- Based in central Cardiff or Caerphilly. Some remote working is available
- Expected employment duration of 3 years
- Flexible working around core hours of 10 – 3
- Reporting to Steph Locke CEO

## The application process
Please send us an email with your CV as an attachment to [people@nightingalehq.ai](mailto://people@nightingalehq.ai).

### CV & submission tips
- There's no need to write a large cover letter for us but if you'd like to put extra info in the email, we recommend taking the headers or criteria from the ad and writing points specific to them
- Please don't include pictures, your address, date of birth, your NI number, your references personal information and so on. The less personally identifiable information we have to store, the better!
- CVs are great when they're one or two pages. We won't discount longer CVs and we will offer CV feedback to any applicant who wishes it. For what Steph, the CEO, thinks a good CV looks like, you can check out [her online CV template](https://www.overleaf.com/articles/steph-lockes-one-page-cv/wbttfmfqcdtq).

### The process
We will be shortlisting applicants and do an online call as a first interview. This first interview will go through the role's criteria and provide a baseline assessment. The next stage will be a face to face interview with the founders to understand what unique perspective you would bring to the role.