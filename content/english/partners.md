---
title: "Become a partner"
aliases:
  - features/partners
menu:
  main:
    weight: 3
    parent: company
---

Our network of services and software partners around the world help businesses achieve success in their data & AI strategies.

## A platform for AI and Data Strategy
Our growing portfolio of tools will support both businesses adopting AI and the local specialists helping them achieve that. 

Today you can register for free to be a part of our AI Connect Lite product - a marketplace and recommendation engine for businesses all over the world to connect to the perfect consultants to support them. 

We're open for sign ups right now from consultants who specialise in helping businesses increase their data or AI maturity.

## Partner criteria

At this point in time we’re looking for consultancies from all over the world who have a data, database, data science, or AI focus. You can be a solo expert or a big consultancy shop. You can specialise in a technology stack, a certain vertical, or a type of challenge. You can do anything from helping people improve their Excel skills to helping them implement a facial recognition system.

The businesses we deal with are all along the spectrum of data & AI maturity so we need partners who can help anywhere along the full spectrum too. The key attribute we’re looking for is a data or AI specialism.

Consultancies who will be part of our enterprise offering AI Connect will go through an extensive vetting process. To meet the criteria for this partner level, you will need to have at least one year of filed accounts and three reference customers.

> Interested in becoming a vetted partner for our enterprise-level AI Connect product? [Contact Us](./contact)


## Join the platform

Join our AI Connect Lite platform today so that we can start matching potential customers with you.

We’ll collect information about these businesses and recommend the most suited partners to them. What this means for you is that we will be showcasing you to the organisations who will find you most beneficial and will therefore convert better into actual clients for you when they reach out.

> Register your business on AI Connect Lite for free. [AI Connect Lite](https://app.nightingalehq.ai)