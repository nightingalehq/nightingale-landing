---
title: Partner
image: /images/uploads/partner.png
description: A partner is a data expert who offers their services via our AI connect platform.
---

A partner is a data expert who offers their services via our AI connect platform. A partner is a business that provides services around data and AI. They are typically consultancies of 1-200 people and have a specialism in a particular area.  

Read more about becoming a partner [here](https://nightingalehq.ai/partners).

