---
title: Machine learning
image: /images/uploads/ai-tag.jpg
description: Machine learning is employed to develop algorithms that can be used to make predictions based on data.
---

Machine learning is employed to develop algorithms that can be used to make predictions based on data.  
