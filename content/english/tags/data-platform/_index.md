---
title: Data Platform
image: /images/uploads/data-collection.jpg
description: Data collection is the systematic process of gathering and measuring information from relevant sources to monitor a situation or answer a question.
---

Data collection is the systematic process of gathering and measuring information from relevant sources to monitor a situation or answer a question.
