---
title: cognitive services
image: /images/uploads/cognitive-services.jpg
description: Azure Cognitive Services is a collection of APIs and services that you can use to integrate intelligent features into your applications. The services are grouped into five categories - decision, language, speech, text, vision, and search.
---

Azure Cognitive Services is a collection of APIs and services that you can use to integrate intelligent features into your applications. The services are grouped into five categories: decision, language, speech, text, vision, and search.
