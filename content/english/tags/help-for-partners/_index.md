---
title: help for partners
image: /images/uploads/edit-profile.png
description: Articles to help partners use and navigate the Nightingale HQ platform.
---

Articles to help partners use and navigate the Nightingale HQ platform. See also [help for organisations](https://nightingalehq.ai/tags/help-for-organisations) and ['What is a partner?'](https://nightingalehq.ai/knowledgebase/app-help/what-is-a-partner).
