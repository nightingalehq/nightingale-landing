---
title: AI
image: /images/uploads/ai-tag.jpg
description: Artificial intelligence is a broad area of computer science concerned with building computer programs that can think like humans.
---

Artificial intelligence is a broad area of computer science concerned with building computer programs that can think like humans.
