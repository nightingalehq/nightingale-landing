---
title: Nightingale HQ app
image: /images/uploads/nhq-logo.png
description: Use the Nightingale platform to learn about artificial intelligence (AI), direct your strategy, and connect with experts.
---

Nightingale HQ is an online platform dedicated to helping businesses adopt Artificial Intelligence (AI) successfully. Via AI Connect Lite, you can connect with the perfect data & AI consultancy to support your AI projects.
